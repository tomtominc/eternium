var dir = argument0;
var cardinal_direction = round(dir/90);

if (cardinal_direction == 1)
{
	return idle_up_sprite;	
}
else if (cardinal_direction == 3)
{
	return idle_down_sprite;
}
else if (cardinal_direction == 2)
{
	scale_x = -1;
	return idle_right_sprite;
}
else 
{
	scale_x = 1;	
	return idle_right_sprite;
}

show_debug_message("Direction = " + string(cardinal_direction));

return idle_right_sprite;