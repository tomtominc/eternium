var sliding = false;

var tiles = ds_list_create();
var tile_count = instance_place_list(x,y,o_tile,tiles, false);
	
for (var i = 0; i < tile_count; i++)
{
	var tile_check = tiles[| 0];
		
	if (tile_check.tile_type == TileType.Wall)
	{
		sliding = true;
		break;
	}
}
		
ds_list_destroy(tiles);

return sliding;