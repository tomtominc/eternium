if (instance_exists(o_dialogue))
{
	o_dialogue.dialogue = argument0;
	o_dialogue.current_index = 0;
	o_dialogue.play = true;
}
else 
{
	with (instance_create_depth(0,0, -100000000, o_dialogue))
	{
		dialogue = argument0;
		current_index = 0;
		play = true;
	}
}