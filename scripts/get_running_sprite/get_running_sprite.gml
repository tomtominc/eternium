var dir = argument0;
var cardinal_direction = round(dir/90);

if (cardinal_direction == 1)
{
	return walk_up_sprite;	
}
else if (cardinal_direction == 3)
{
	return walk_down_sprite;
}
else if (cardinal_direction == 2)
{
	scale_x = -1;
	return walk_right_sprite;
}
else 
{
	scale_x = 1;	
	return walk_right_sprite;
}

return walk_right_sprite;