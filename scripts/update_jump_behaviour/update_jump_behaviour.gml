move_z -= grav;

// I'm landing!

if (z + move_z <= 0)
{	
	z_ground--;
	
	if (is_wall_sliding())
	{
		move_y = -move_z;
		state = State.Wall_Sliding;
	}
	else if (get_distance_to_ground() > 0)
	{
		move_y = -move_z;
		state = State.Falling;
		fall_start_y = y;
	}
	else 
	{
		state = State.Idle;	
		z_ground++;
		
		if (!is_masked_by_tile())
		{
			z_ground = get_ground();
		}
		else 
		{
			z_ground--;	
		}
	}
	
	move_z = 0;
	z = 0;
}
else 
{
	z += move_z;
	
	move_x = lengthdir_x(input_magnitude * move_speed, input_direction);
	move_y = lengthdir_y(input_magnitude * move_speed, input_direction);
	
	entity_collisions();
	
	/*var ground_check = get_ground();
	
	if (ground_check == z_ground+1)
	{
		z_ground = ground_check;	
	}*/
}
