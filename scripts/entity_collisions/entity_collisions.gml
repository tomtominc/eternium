
var collided = false;
//===================================================================================================
// COLLISION SETUP
//===================================================================================================
var collision_list = ds_list_create();
var collision_count = 0;

//===================================================================================================
// HANDLE HORIZONTAL COLLISIONS
//===================================================================================================
if (move_x > 0)
{
	collision_count = collision_rectangle_list(bbox_right+1,bbox_top,bbox_right+1,bbox_bottom, o_tile, false, false, collision_list, false);
}

if (move_x < 0)
{
	collision_count = collision_rectangle_list(bbox_left-1,bbox_top,bbox_left-1,bbox_bottom, o_tile, false, false, collision_list, false);
}

if (collision_count <= 0 && z_ground > 0 && state == State.Idle)
{
	move_x = 0;	
	collided = true;
}

for (var i = 0; i < collision_count; i++)
{
	var tile_check = collision_list[| i];
	
	if (!tile_check.collides)
		continue;
	
	// In the jump state I don't collide with tiles with less height than me (because I'm jumping over them!)
	if (state == State.Jumping)
	{
		if (tile_check.height >= z_ground+1)
		{
			move_x = 0;
			collided = true;
			break;
		}
		
	}
	// In the falling state I don't collide with walls or anything with height that is lower than mine
	else if (state == State.Falling || state == State.Wall_Sliding)
	{
		if (tile_check.height > z_ground && tile_check.tile_type != TileType.Wall)
		{
			move_x = 0;
			collided = true;
			break;
		}
	}
	// In the idle state I collide with everything that isn't my height + walls
	else
	{
		if (tile_check.height != z_ground || tile_check.tile_type == TileType.Wall)
		{
			move_x = 0;
			collided = true;
			break;
		}
	}
}

//===================================================================================================
// HANDLE VERTICAL COLLISIONS
//===================================================================================================
if (move_y < 0)
{
	collision_count = collision_rectangle_list(bbox_left,bbox_top-1,bbox_right,bbox_top-1, o_tile, false, false, collision_list, false);
}

if (move_y > 0)
{
	collision_count = collision_rectangle_list(bbox_left,bbox_bottom+1,bbox_right,bbox_bottom+1, o_tile, false, false, collision_list, false);
}

if (collision_count <= 0 && z_ground > 0 && state == State.Idle)
{
	move_y = 0;	
	collided = true;
}

for (var i = 0; i < collision_count; i++)
{
	var tile_check = collision_list[| i];
	
	if (!tile_check.collides)
		continue;
	
	// In the jump state I don't collide with tiles with less height than me (because I'm jumping over them!)
	if (state == State.Jumping)
	{
		if (tile_check.height >= z_ground+1)
		{
			move_y = 0;
			collided = true;
			break;
		}
	}
	// In the falling state I don't collide with walls or anything with height that is lower than mine
	else if (state == State.Falling|| state == State.Wall_Sliding)
	{
		if (tile_check.height > z_ground && tile_check.tile_type != TileType.Wall)
		{
			move_y = 0;
			collided = true;
			break;
		}
	}
	// In the idle state I collide with everything that isn't my height + walls
	else
	{
		if (tile_check.height != z_ground || tile_check.tile_type == TileType.Wall)
		{
			move_y = 0;
			collided = true;
			break;
		}
	}
}


//===================================================================================================
// COMMIT TO MOVEMENT
//===================================================================================================
x += move_x;
y += move_y;


//===================================================================================================
// COLLISION CLEAN UP
//===================================================================================================
ds_list_destroy(collision_list);

return collided;