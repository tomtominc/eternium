var old_sprite = sprite_index;

if (state == State.Idle)
{
	if (input_magnitude != 0)
	{
		facing_direction = input_direction;
		sprite_index = get_running_sprite(facing_direction);
		
		/*if (frame % 12 == 0)
		{
			var dust_distance = -8;
			var dust_x =x + lengthdir_x(dust_distance, input_direction);
			var dust_y =y+ lengthdir_y(dust_distance, input_direction);
			var dust = instance_create_layer(dust_x,dust_y,"Player",o_dust);
		
			dust.depth = depth - 2;
		}*/
	}
	else 
	{
		sprite_index = get_idle_sprite(facing_direction);
	}
}
else if (state == State.Jumping)
{	
	sprite_index = get_jump_sprite(facing_direction);
	
	if (move_z > 0)
	{
		image_index = 1;
	}
	else if (z < 5) 
	{
		image_index = 2;
	}
	else 
	{
		image_index = 2;	
	}
}
else if (state == State.Falling)
{
	sprite_index = get_jump_sprite(facing_direction);
	image_index = 2;	
}
else if (state == State.Melee_Sword)
{
	sprite_index = s_player_melee_sword_down;	
}


if (old_sprite != sprite_index)
{
	image_index = 0;
}

