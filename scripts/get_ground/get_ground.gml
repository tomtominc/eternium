
if (place_meeting(x,y,o_tile))
{
	var tiles = ds_list_create();
	var tileCount = instance_place_list(x,y,o_tile,tiles, false);
	var current_height = 0;
		
	for (var i = 0; i < tileCount; i++)
	{
		var tile = tiles[| i];
		
		if (!tile.collides)
			continue;
			
		if (tile.height > current_height)
		{
			current_height= tile.height;
		}
	}
			
	return current_height;
	
	ds_list_destroy(tiles);
}

return 0;

