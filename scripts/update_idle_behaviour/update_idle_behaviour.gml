move_x = lengthdir_x(input_magnitude * move_speed, input_direction);
move_y = lengthdir_y(input_magnitude * move_speed, input_direction);

z = 0;
	
if (key_jump)
{
	move_z = jump_height;
	z = move_z;
	state = State.Jumping;
	z_ground++;
	z_ground_shadow = z_ground;
	shadow.visible = true;	
}
else if (key_roll)
{
	state = State.Roll;
	roll_distance_remaining = roll_distance;
	roll_direction = facing_direction;
	roll_speed_current = roll_speed;
	roll_end_wait_delay_current = roll_end_wait_delay;
	move_z = 0;
	z = roll_height;
	
}
else if (key_dialogue)
{	
	state = State.Dialogue;
	
	var message_map = create_dialogue();
	add_dialogue(message_map, s_player_portrait, "This is message 1");
	add_dialogue(message_map, s_player_portrait, "This is message 2");
	add_dialogue(message_map, s_player_portrait, "This is message 3");
	play_dialogue(message_map);
}
else if (key_attack)
{
	//state = State.Melee_Sword;	
}
else 
{
	move_z = 0;
}

entity_collisions();