var height = argument0;
var color = c_white;
	
if (height == 1)
{
	color = c_yellow;	
}
else if (height == 2)
{
	color = c_orange;	
}
else if (height == 3)
{
	color = c_red;
}
else if (height >= 4)
{
	color = c_purple;
}
	
draw_collision_box(color);