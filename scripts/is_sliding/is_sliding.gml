input_direction = point_direction(0,0,key_right - key_left, key_down - key_up);
input_magnitude = (key_right - key_left != 0) || (key_down - key_up != 0);

move_x = lengthdir_x(input_magnitude * move_speed, input_direction);
move_y = lengthdir_y(input_magnitude * move_speed, input_direction);

//===================================================================================================
// COLLISION SETUP
//===================================================================================================
var collision_list = ds_list_create();
var collision_count = 0;

//===================================================================================================
// HANDLE HORIZONTAL COLLISIONS
//===================================================================================================
if (move_x > 0)
{
	collision_count = collision_rectangle_list(bbox_right+1,bbox_top,bbox_right+1,bbox_bottom, o_tile, false, false, collision_list, false);
}

if (move_x < 0)
{
	collision_count = collision_rectangle_list(bbox_left-1,bbox_top,bbox_left-1,bbox_bottom, o_tile, false, false, collision_list, false);
}

for (var i = 0; i < collision_count; i++)
{
	var tile_check = collision_list[| i];
	
	// In the jump state I don't collide with tiles with less height than me (because I'm jumping over them!)
	if (state == State.Jumping)
	{
		if (tile_check.height > z_ground)
		{
			move_x = 0;
			break;
		}
	}
	// In the falling state I don't collide with walls or anything with height that is lower than mine
	else if (state == State.Falling)
	{
		if (tile_check.height > z_ground)
		{
			move_x = 0;
			break;
		}
	}
	// In the idle state I collide with everything that isn't my height + walls
	else if (state == State.Idle)
	{
		if (tile_check.height != z_ground)
		{
			move_x = 0;
			break;
		}
	}
}

//===================================================================================================
// HANDLE HORIZONTAL COLLISIONS
//===================================================================================================
if (move_y < 0)
{
	collision_count = collision_rectangle_list(bbox_left,bbox_top-1,bbox_right,bbox_top-1, o_tile, false, false, collision_list, false);
}

if (move_y > 0)
{
	collision_count = collision_rectangle_list(bbox_left,bbox_bottom+1,bbox_right,bbox_bottom+1, o_tile, false, false, collision_list, false);
}

for (var i = 0; i < collision_count; i++)
{
	var tile_check = collision_list[| i];
	
	// In the jump state I don't collide with tiles with less height than me (because I'm jumping over them!)
	if (state == State.Jumping)
	{
		if (tile_check.height > z_ground)
		{
			move_y = 0;
			break;
		}
	}
	// In the falling state I don't collide with walls or anything with height that is lower than mine
	else if (state == State.Falling)
	{
		if (tile_check.height > z_ground)
		{
			move_y = 0;
			break;
		}
	}
	// In the idle state I collide with everything that isn't my height + walls
	else if (state == State.Idle)
	{
		if (tile_check.height != z_ground)
		{
			move_y = 0;
			break;
		}
	}
}


//===================================================================================================
// COMMIT TO MOVEMENT
//===================================================================================================
x += move_x;
y += move_y;


//===================================================================================================
// COLLISION CLEAN UP
//===================================================================================================
ds_list_destroy(collision_list);