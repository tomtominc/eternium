move_x = lengthdir_x(input_magnitude * move_speed, input_direction);
move_y += grav;

entity_collisions();

var fall_distance_traveled = y - fall_start_y;
		
if (fall_distance_traveled >= tile_size)
{
	fall_start_y = y;
	z_ground--;
	
	if (z_ground < 0)
	{
		z_ground = 0;
		state = State.Idle;
	}
}
		
if (get_distance_to_ground() <= 0)
{
	state = State.Idle;	
	z_ground = get_ground();
}