var state = argument0;

switch (state)
{
	case State.Idle: return "Idle"; break;
	case State.Jumping: return "Jump"; break;
	case State.Falling: return "Fall";break;
	case State.Wall_Sliding: return "Slide"; break;
	case State.Melee_Sword: return "Melee (Sword)";break;
	case State.Roll: return "Roll"; break;
}

return "Unknown";