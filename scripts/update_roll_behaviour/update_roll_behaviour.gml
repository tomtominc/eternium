move_x = lengthdir_x(roll_speed_current, roll_direction);
move_y = lengthdir_y(roll_speed_current, roll_direction);

var collided = entity_collisions();

roll_speed_current = max(0, roll_speed_current - grav);
roll_distance_remaining = max(0, roll_distance_remaining - roll_speed_current);

sprite_index = get_roll_animation(roll_direction);

move_z -= grav;
z += move_z;

if (z <= 0)
{
	z = 0;	
}

if (roll_distance_remaining <= 0 || roll_speed_current <= 0)
{
	roll_end_wait_delay_current--;
	
	if (roll_end_wait_delay_current <= 0)
	{
		state = State.Idle;
	}	
}

