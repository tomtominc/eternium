var covered = false;
var original_mask = mask_index;
mask_index = s_player_tile_mask;

if (place_meeting(x,y,o_tile))
{
	var collision_list = ds_list_create();
	var collision_count = instance_place_list(x,y,o_tile,collision_list,false);
	
	for (var i = 0; i < collision_count; i++)
	{
		var tile_check = collision_list[| i];
		
		if (tile_check.tile_type == TileType.Wall)
			continue;
		
		if (tile_check.height > z_ground)
		{
			covered = true;
			break;
		}
	}
	
	ds_list_destroy(collision_list);
}

mask_index = original_mask;

return covered;