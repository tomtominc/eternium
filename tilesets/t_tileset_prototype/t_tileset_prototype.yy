{
    "id": "a3d49348-91f9-407a-a67f-e52c12c02735",
    "modelName": "GMTileSet",
    "mvc": "1.11",
    "name": "t_tileset_prototype",
    "auto_tile_sets": [
        
    ],
    "macroPageTiles": {
        "SerialiseData": null,
        "SerialiseHeight": 0,
        "SerialiseWidth": 0,
        "TileSerialiseData": [
            
        ]
    },
    "out_columns": 4,
    "out_tilehborder": 0,
    "out_tilevborder": 0,
    "spriteId": "295681ec-f6f8-47c4-bf39-060f931ea998",
    "sprite_no_export": true,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "tile_animation": {
        "AnimationCreationOrder": null,
        "FrameData": [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15
        ],
        "SerialiseFrameCount": 1
    },
    "tile_animation_frames": [
        
    ],
    "tile_animation_speed": 15,
    "tile_count": 16,
    "tileheight": 8,
    "tilehsep": 0,
    "tilevsep": 0,
    "tilewidth": 8,
    "tilexoff": 0,
    "tileyoff": 0
}