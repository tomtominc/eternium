/// @description Insert description here
// You can write your code in this editor

heightmap = layer_tilemap_get_id("Heightmap");
ground_layer = layer_tilemap_get_id("GroundLayer");
current_height = 0;
last_ground_height = 0;
tile_type = TileType.Ground;
tile_deepness = 0;


for (var c = 0; c < tilemap_get_width(heightmap); c++)
{
	for (var r = tilemap_get_height(heightmap); r > 0; r--)
	{
		var tile = tilemap_get(heightmap, c, r);
		
		if (tile == 0)
		{
			if (tile_type == TileType.Wall)
			{
				current_height++;
			}
		}
		else if (tile == 13)
		{
			tile_type = TileType.Ground;
			current_height = last_ground_height;
		}
		else if (tile == 14)
		{
			if (tile_type == TileType.Wall)
			{
				tile_type = TileType.Ground;
				tile_deepness = 1;
			}
			else 
			{
				tile_type = TileType.Wall;
				last_ground_height = current_height;
				current_height++;
			}
		}
		
		if (current_height > 0)
		{
			var tile_instance = instance_create_layer(c*tile_size,r*tile_size,"TileLayer",o_tile);
			var ts = tilemap_get_tileset(ground_layer);
			var tile_ground = tilemap_get(ground_layer,c,r);
			
			with (tile_instance)
			{
				height = other.current_height;
				tileset = ts;
				tile_index = tile_get_index(tile_ground);
				tile_type = other.tile_type;
				depth = floor((ground_depth - (other.current_height*tile_size)) - (tile_size * (r+1)));
				
				if (tile_type == TileType.Wall)
				{
					collides = true;
				}
			}
			
			if (!tile_get_empty(tile_ground))
			{
				tile_ground = tile_set_empty(tile_ground);
				tilemap_set(ground_layer, tile_ground, c, r);
			}
		}
	}
	
	current_height = 0;
	tile_type = TileType.Ground;
}

iterate_height = 0;
ground_length = 0;

for (var c = 0; c < tilemap_get_width(heightmap); c++)
{
	for (var r = tilemap_get_height(heightmap); r > 0; r--)
	{
		var tile = tilemap_get(heightmap, c, r);
		
		if (tile == 0)
		{
			if (tile_type == TileType.Wall)
			{
				iterate_height++;
			}
			else if (ground_length > 0)
			{
				ground_length++;
			}
		}
		else if (tile == 13)
		{
			var pos_x = c;
			var pos_y = r+1;
			
			for (var i = 0; i < ground_length; i++)
			{
				var temp = instance_position(pos_x * tile_size, pos_y * tile_size, o_tile);
				
				with (temp)
				{
					seen = true;
					
					if (tile_type == TileType.Ground)
					{
						collides = true;
						ledge_deepness = i;
						pos_y++;
					}
				}
			}
			
			ground_length = 0;
			iterate_height = 0;
			tile_type = TileType.Ground;
		}
		else if (tile == 14)
		{
			if (tile_type == TileType.Ground)
			{
				iterate_height++;
				tile_type = TileType.Wall;
				
				if (ground_length > 0)
				{
					var pos_x = c;
					var pos_y = r + iterate_height - 1;
			
					for (var i = 0; i < ground_length; i++)
					{
						var temp = instance_position(pos_x * tile_size, pos_y * tile_size, o_tile);
				
						with (temp)
						{
							if (temp.tile_type == TileType.Ground)
							{
								temp.collides = true;	
								pos_y++;
							}
						}
					}
			
					ground_length = 0;
				}
			}
			else
			{
				ground_length++;
				tile_type = TileType.Ground;
			}
		}
	}
	
	iterate_height = 0;
	ground_length = 0;
	tile_type = TileType.Ground;
}

