/// @description Insert description here
// You can write your code in this editor

update_input();

input_direction = point_direction(0,0,key_right - key_left, key_down - key_up);
input_magnitude = (key_right - key_left != 0) || (key_down - key_up != 0);

if (state == State.Jumping)
{
	update_jump_behaviour();
}
else if (state == State.Idle)
{
	update_idle_behaviour();
}
else if (state == State.Falling)
{
	update_falling_behaviour();
}
else if (state == State.Wall_Sliding)
{
	update_wall_sliding_behaviour();
}
else if (state == State.Roll)
{
	update_roll_behaviour();
}
else if (state == State.Dialogue)
{
	update_dialogue_behaviour();	
}
if (is_masked_by_tile())
{
	depth = floor((ground_depth - (z_ground*tile_size)) - (y-z-1+tile_size));
	using_masked_depth = true;
}
else 
{
	depth = floor((ground_depth - ((z_ground+2)*tile_size)) - (y-z));
	using_masked_depth = false;
}


animate();