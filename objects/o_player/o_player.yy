{
    "id": "cbfcca29-62ad-4846-a652-81f6cc64990e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_player",
    "eventList": [
        {
            "id": "d812c169-fe97-46ad-a0f8-ca1c240de4a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cbfcca29-62ad-4846-a652-81f6cc64990e"
        },
        {
            "id": "990770a1-dcb2-4b06-9d8d-9774a0baa0b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cbfcca29-62ad-4846-a652-81f6cc64990e"
        },
        {
            "id": "e643ae12-120a-4fcc-908f-615868371a70",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "cbfcca29-62ad-4846-a652-81f6cc64990e"
        },
        {
            "id": "5f7db8ab-c084-4777-8f78-152eebe6f286",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "cbfcca29-62ad-4846-a652-81f6cc64990e"
        }
    ],
    "maskSpriteId": "323f9448-f28d-43c1-a6ac-20a0f54d160a",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "323f9448-f28d-43c1-a6ac-20a0f54d160a",
    "visible": true
}