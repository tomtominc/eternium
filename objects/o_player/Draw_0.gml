/// @description Insert description here
// You can write your code in this editor



if (state == State.Jumping)
{
	var ground_below = get_ground();
	shadow.depth = floor((ground_depth - ((z_ground_shadow+2)*tile_size)) - (y+tile_size));
	shadow.x = x;
	shadow.y = y - ((ground_below - (z_ground_shadow-1)) * tile_size);
	shadow.visible = true;
}
else if (state == State.Roll)
{
	shadow.depth = depth + tile_size;
	shadow.x = x;
	shadow.y = y;
	shadow.visible = true;
}
else 
{
	draw_sprite(s_shadow_16x16,0,x,y);
	shadow.visible = false;
}

draw_sprite_ext(sprite_index,image_index, x,y-z, scale_x, 1, 0, c_white, 1);


if (keyboard_check_pressed(ord("H")))
{
	is_hud_active = !is_hud_active;	
}

if (keyboard_check(vk_shift) && keyboard_check_pressed(ord("S")))
{
	if (room_speed < original_room_speed)
	{
		room_speed = original_room_speed;	
	}
	else 
	{
		room_speed = 10;	
	}
}

