/// @description Insert description here
// You can write your code in this editor

if (global.show_debug)
{
	draw_collision_box_height(z_ground);
}

if (is_hud_active)
{
	draw_set_color(c_black);
	draw_rectangle(2,2,64,41, false);
	draw_set_color(c_white);
	draw_set_font(fnt_kobold);

	var line_height = 9;
	var lines = 0;
	draw_text(4,line_height * lines, "State: " + get_state_string(state));
	lines++;
	draw_text(4,line_height * lines, "Height: " + string( z_ground));
	lines++;
	draw_text(4,line_height * lines,"D" + string(depth) + "Z" + string(z) + "Y"+string(y));
	lines++
	if(using_masked_depth)
		draw_text(4,line_height * lines, "Masked: Y");
	else 
		draw_text(4,line_height * lines, "Masked: N");
}