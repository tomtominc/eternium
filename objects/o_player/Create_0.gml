/// @description Insert description here
// You can write your code in this editor

enum State 
{
	Idle,
	Jumping,
	Falling,
	Wall_Sliding,
	Melee_Sword,
	Roll,
	Dialogue,
}

enum Direction
{
	Right,
	Down,
	Left,
	Up
}

state = State.Idle;
move_speed = 1;
grav = 0.12;
z = 0;
z_ground = 0;
z_ground_shadow = 0;
move_x = 0;
move_y = 0;
move_z = 0;
jump_height = 2;
input_direction = 0;
input_magnitude = 0;
facing_direction = 0;
scale_x = 1;
fall_start_y = 0;
ground_layer = layer_tilemap_get_id(layer_get_id("GroundLayer"));
shadow = instance_create_layer(x,y,"Shadow",o_shadow);
shadow.visible = false;
is_hud_active = true;
original_room_speed = room_speed;
using_masked_depth = false;

// Roll State
roll_speed = 4;
roll_speed_current = 0;
roll_distance = 64;
roll_direction = 0;
roll_distance_remaining = 0;
roll_end_wait_delay = 12;
roll_end_wait_delay_current = 0;
roll_height = 16;

//room_speed =10;


// Sprites

idle_down_sprite = s_player_idle_down;
idle_up_sprite = s_player_idle_up;
idle_right_sprite = s_player_idle_right;

walk_down_sprite = s_player_walk_down;
walk_up_sprite = s_player_walk_up;
walk_right_sprite = s_player_walk_right;

jump_down_sprite = s_player_jump_down;
jump_up_sprite = s_player_jump_up;
jump_right_sprite = s_player_jump_right;

melee_sword_down_sprite = s_player_melee_sword_down;
melee_sword_up_sprite = s_player_melee_sword_up;
melee_sword_right_sprite = s_player_melee_sword_right;

roll_down_sprite = s_player_roll_down;
roll_up_sprite = s_player_roll_up;
roll_right_sprite = s_player_roll_right;