{
    "id": "dc126df2-2f85-4d69-b37e-4f99ebda330d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_dust",
    "eventList": [
        {
            "id": "bc2934d7-c17b-4bd8-a83f-8fadc26ac8df",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "dc126df2-2f85-4d69-b37e-4f99ebda330d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "85c24e27-f3fd-42c0-b7f2-b7de3db6da9d",
    "visible": true
}