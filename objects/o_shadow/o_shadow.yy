{
    "id": "694495ea-2378-4128-b81a-5555805220e2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_shadow",
    "eventList": [
        {
            "id": "45ef57e1-b07f-4d50-9ab1-a5e2747d3510",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "694495ea-2378-4128-b81a-5555805220e2"
        },
        {
            "id": "6c64b782-1774-4c2b-b5ae-50a9190d5a05",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "694495ea-2378-4128-b81a-5555805220e2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "83e2fbd4-42c9-42ba-9c31-6e4518f1e65f",
    "visible": true
}