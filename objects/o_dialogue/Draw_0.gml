/// @description Insert description here
// You can write your code in this editor

if (play)
{
	var portrait_list = ds_map_find_value(dialogue, "portrait");
	
	if (ds_list_size(portrait_list) > current_index)
	{
		portrait = ds_list_find_value(portrait_list, current_index);
	}
	
	var message_list =  ds_map_find_value(dialogue, "message");
	
	if (ds_list_size(message_list) > current_index)
	{
		message = ds_list_find_value(message_list, current_index);
	}
	else 
	{
		play = false;	
	}
	
	if (play)
	{
		var vx = 0;
		var vy = 0;
		var vw = vx + screen_width;
		var vh = vy + screen_height;
		
		draw_set_color(c_black);
		draw_rectangle(vx,vh-52,vw,vh, false);
		draw_set_color(c_white);
		draw_set_font(fnt_kobold);
		
		draw_sprite(portrait,0,vx+2, vh-50);
		draw_text(vx+52,vh-50, message);
	}
}