{
    "id": "dc6f10c5-f611-4235-b3e7-54fcc3ecbfbf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_tile",
    "eventList": [
        {
            "id": "4044fe75-6fd5-4808-8db5-c0cc777e49c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dc6f10c5-f611-4235-b3e7-54fcc3ecbfbf"
        },
        {
            "id": "b8cc3806-352e-401d-8443-9bec2b2b69c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "dc6f10c5-f611-4235-b3e7-54fcc3ecbfbf"
        },
        {
            "id": "eacca020-68fa-4998-83fc-72854e5f07a8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dc6f10c5-f611-4235-b3e7-54fcc3ecbfbf"
        }
    ],
    "maskSpriteId": "6cd02cf1-9aff-45ec-9a2f-59b3f45acec6",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}