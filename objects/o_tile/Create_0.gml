/// @description Insert description here
// You can write your code in this editor

enum TileType
{
	Ground,
	Wall
}

enum DebugInfo
{
	Deepness,
	Type,
	Seen,
	Collision,
}

tile_type = TileType.Ground;
height = 0;
tileset = -1;
tile_index = -1;
collides = false;
ledge_deepness = -1;
debug_info = DebugInfo.Deepness;
seen = false;