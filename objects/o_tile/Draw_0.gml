/// @description Insert description here
// You can write your code in this editor

if (tileset < 0 || tile_index < 0)
	return;

draw_tile(tileset,tile_index,0,x,y);

if (global.show_debug)
{
	//draw_collision_box_height(height);
	
	if (keyboard_check_pressed(vk_add))
	{
		if (debug_info == DebugInfo.Deepness)
		{
			debug_info = DebugInfo.Type;
		}
		else if (debug_info = DebugInfo.Type)
		{
			debug_info = DebugInfo.Seen;
		}
		else if (debug_info == DebugInfo.Seen)
		{
			debug_info = DebugInfo.Collision;	
		}
		else if (debug_info == DebugInfo.Collision)
		{
			debug_info = DebugInfo.Deepness;	
		}
	}
	
	var color = c_black;
	
	if (tile_type == TileType.Wall)
	{
		color = c_white;	
	}
	
	draw_set_color(color);
	draw_set_font(fnt_kobold);
	
	if (debug_info == DebugInfo.Deepness)
	{
		draw_text(x,y-4, string(ledge_deepness));
	}
	else if (debug_info = DebugInfo.Type)
	{
		draw_text(x,y-4, string(depth));
	}
	else if (debug_info == DebugInfo.Seen)
	{
		color = c_teal;
		
		if (seen == false)
		{
			color = c_red;	
		}
		
		draw_set_color(color);
		draw_set_alpha(0.5);
		draw_rectangle(bbox_left, bbox_top, bbox_right, bbox_bottom,false);
		draw_set_alpha(1);
	}
	else if (debug_info == DebugInfo.Collision)
	{
		if (collides)
		{
			color = c_lime;
			draw_set_color(color);
			draw_set_alpha(0.5);
			draw_rectangle(bbox_left, bbox_top, bbox_right, bbox_bottom,false);
			draw_set_alpha(1);
		}
		
	}
}
