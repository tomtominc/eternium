{
    "id": "82a60d91-0e37-4b5d-a770-77dfdf87dbe3",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_kobold",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "KoboldPro",
    "glyphOperations": 3,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "f39e3672-ad42-4b23-adbe-6f290d6da828",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "2f20f16b-2922-4de3-9362-46846fbb7838",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 113,
                "y": 30
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "7503e4d1-d2b6-4fae-b086-83b78982dc9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 108,
                "y": 30
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "0a547b32-a09a-4cf2-a756-5300315f9866",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 102,
                "y": 30
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "93ccaeb9-626c-4894-a698-eb1f64a273e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 96,
                "y": 30
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "6e7dd6f3-302e-4b0e-96e1-8ffce9a41cd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 91,
                "y": 30
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "337951ca-7aae-4c0a-a43c-8f99cf3e059b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 85,
                "y": 30
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "f929fd52-5d22-4c11-b4fc-e1c614dca36c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 82,
                "y": 30
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "c4d7eb74-edff-4c16-a229-7ac73258ad6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 77,
                "y": 30
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "5e0aabee-0885-4604-b729-b8bb34b29c64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 72,
                "y": 30
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "a91bc58c-bbce-4133-a9fe-e7e1b034f230",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 116,
                "y": 30
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "54bfaa2f-4d55-417e-aa4f-33709dfcd5cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 67,
                "y": 30
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "5062242d-56fe-49b7-bedd-5dfd4af90f3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 58,
                "y": 30
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "4c27591d-38e9-4e3b-9759-3cb2e969fd65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 53,
                "y": 30
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "5fd13892-1ab0-4c09-a8e5-aa966927c8cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 50,
                "y": 30
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "47823f07-686f-4027-a86c-9792ebcbe688",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 45,
                "y": 30
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "88488669-6a00-4cdc-8ec9-6b14a8368d16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 40,
                "y": 30
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "37d9b654-6e6b-437f-9300-7d7a144ac5b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 35,
                "y": 30
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "efca3821-6dc3-4ab3-8631-56a249dc1020",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 30,
                "y": 30
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "fb25b8eb-83ad-4278-acbe-235ac5b90a07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 25,
                "y": 30
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "32699c80-75d4-411d-aef0-9cedeff79a5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 20,
                "y": 30
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "8ce1f7c9-b7ac-4147-a711-9bebd0895b12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 62,
                "y": 30
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "5723bb0a-b221-4d65-964b-a732dce1d03f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 121,
                "y": 30
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "5e8c6f12-d59f-422c-ab85-7cd7577b3c31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "125a58a6-1065-4f5f-ac8d-cc8cc197a606",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 7,
                "y": 44
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "65831153-ffe1-47db-b347-92c19fc2fea0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 120,
                "y": 44
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "7c048c26-e776-47fc-bfab-80a33ee8a14d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 117,
                "y": 44
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "0d208a83-ed68-42e8-a2ce-94634ea76ca7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 113,
                "y": 44
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "c9d34bf3-68db-40fa-bc57-9fe9b2d350b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 109,
                "y": 44
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "a9850e0d-83d4-4c0d-9c6e-edc9bb1e6306",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 104,
                "y": 44
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "a08dcd5a-31d7-411c-a6d4-3dd4117328ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 100,
                "y": 44
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "b46342c0-e85b-4cb2-b94e-2abb003e7af1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 95,
                "y": 44
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "825d7797-3d36-4827-8e84-dc2e8fdb6e02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 88,
                "y": 44
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "dc3089d0-3ef6-4254-9577-c14e45bcda83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 82,
                "y": 44
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "1bc54bdd-b6d1-4dfa-b3c2-d0a5ff40a59b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 76,
                "y": 44
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "fdd474e2-809f-473a-b4aa-7994762d9781",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 70,
                "y": 44
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "df449120-308f-401d-b4d9-40facaba8bef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 64,
                "y": 44
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "dbc253e9-d1d7-455a-bf02-db3315c970d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 58,
                "y": 44
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "b9858329-7fb8-4d30-b836-668b1102aeb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 53,
                "y": 44
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "e2994a61-f475-4af1-bdbb-cd8b7194d398",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 47,
                "y": 44
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "e5b03b9c-4670-4b31-9c6e-69539e254ec6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 41,
                "y": 44
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "21a162a8-9308-4e22-91a7-75d48534e2de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 36,
                "y": 44
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "5e73f1c9-4a54-438c-b637-572d10e7dd7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 31,
                "y": 44
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "0a71ae78-6def-420c-8d2a-03e6cbf3ae01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 25,
                "y": 44
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "3d5505ad-5345-4bc8-81ef-52c8936be9b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 19,
                "y": 44
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "fb6f3bab-c382-4a32-b9e8-5c137e97f64c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 12,
                "y": 44
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "5be18479-6fa1-4503-833a-09eac7655c01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 14,
                "y": 30
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "f0b9cda5-b965-4e38-b63b-9c857d4b20d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 8,
                "y": 30
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "89f9d8eb-b537-41b2-9ad5-ccd150268d04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 2,
                "y": 30
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "26725ffa-7d70-4033-974f-89dc452285f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 121,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "02b4696e-63f7-4333-a508-dc1367cf4a41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "bf43fac8-a456-457d-a443-62f3c11f539b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "1fbb622a-695f-4554-800a-a6435ec30898",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "47798e4e-6bb2-46da-bcf6-3fc611930b07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "24bc7cdc-7411-4006-9a25-9991f43c890e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "2f64a1cf-e6be-4cfc-8c96-c095ad33d556",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "80374cdd-a993-4b8e-ab57-f4f2e497a1ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "21632650-5283-4ef5-9db5-ec928fdd5273",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 69,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "d00e16a2-4fee-412a-af1c-2e54b417ac13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 63,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "a059823e-7295-43d9-8eba-495a26814274",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "a22f7d72-a1a3-4059-b51b-7c1fb809fbcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "3a4e7e0c-cbc6-46dc-ad72-96dd40dd6ae5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "8ec40ffb-c154-40f8-9655-fa7a2666cb6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "442b183f-0de7-4b58-a0ee-d92a74665753",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 38,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "ca9b6548-95f9-4738-acde-18805da5b82b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "54250776-0eb4-47a9-9f12-9f9920063313",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 29,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "c978399a-9e03-4b3d-9195-2b378fd9f92d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "24938bfe-4001-428d-a13b-fa31eda438b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 19,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "29735695-a2f1-4269-aee8-d7711955c5ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "8826232d-d9ee-4986-9fec-31a04cbf5442",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "b29fbd9d-e660-4842-a6ef-f572a4afb336",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "3c0d30f7-f438-4b42-b43b-f032d1ad19ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 2,
                "y": 16
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "7ef94f44-36fe-4729-a7ef-5ea826603ad4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 58,
                "y": 16
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "c8dbd919-d2ab-450e-af84-1d7e128ef717",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 7,
                "y": 16
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "0a60cb79-3a86-4395-95e0-48728e2cb0be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 113,
                "y": 16
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "82c6ff16-d7ae-4d0c-9eb2-aa7fb2b61da2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 108,
                "y": 16
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "0b09d404-167e-4888-b1fa-3a6d5b810689",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 103,
                "y": 16
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "067760b3-4d19-43c8-90a1-d792b412ffa4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 96,
                "y": 16
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "c5385e33-e8ed-405f-a1a2-7a049e0e58a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 90,
                "y": 16
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "927fc998-82df-4e2e-8292-6bd450747794",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 85,
                "y": 16
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "0610d518-0bd7-4541-92ba-ce0bb2e7a2dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 79,
                "y": 16
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "201338f0-0d16-45cd-a04d-d4a50e1a559f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 74,
                "y": 16
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "98b5e0f5-1fa7-47ce-8e2e-0b5fd648906f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 68,
                "y": 16
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "e7d637d2-3642-474f-aa9b-78f0e20ef01a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 118,
                "y": 16
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "73f6c291-77bb-4f10-a811-f346ae0d19e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 63,
                "y": 16
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "7d3099c3-0962-49a7-82eb-9e047707a1ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 53,
                "y": 16
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "be41a8f0-53f9-4734-9051-3541a5253e9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 47,
                "y": 16
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "4dfd2107-dd39-462d-b60c-7bef83cc4a02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 40,
                "y": 16
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "fd8f7ac8-c56f-4452-a48a-82135a3eb32d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 35,
                "y": 16
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "8921d193-abcc-43fc-a0d5-f835af483e53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 30,
                "y": 16
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "0075e50b-d9a7-44ed-85d7-817d15bfbc2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 25,
                "y": 16
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "77878cf6-8f3d-47d2-ac2a-df38937a8843",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 20,
                "y": 16
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "778acdbf-6207-4dc0-a043-73c8842b536b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 17,
                "y": 16
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "22b91067-938a-4bf6-bef7-289b5cd5fe3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 12,
                "y": 16
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "57fab518-61c5-4927-94d8-26cfc7bea907",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 3,
                "x": 2,
                "y": 58
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "5162ab23-6341-4b7f-8811-6c1e4ee44321",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 12,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 7,
                "y": 58
            }
        }
    ],
    "hinting": 2,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 2,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\n0123456789 .,<>\"'&!?\\nthe quick brown fox jumps over the lazy dog\\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\nDefault character: ▯ (9647)",
    "size": 10,
    "styleName": "Medium",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}