{
    "id": "82a60d91-0e37-4b5d-a770-77dfdf87dbe3",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_kobold",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "KoboldPro",
    "glyphOperations": 3,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "1b1feb57-8598-426d-918a-b800e09b64a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "9d64550f-bb03-4a91-a528-982ea596495b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 14,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 37,
                "y": 50
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "929fd4ef-cf9f-4a3b-b324-8326020e2ce1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 31,
                "y": 50
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "394a1235-bb4a-463d-b494-f265601ed858",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 24,
                "y": 50
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "1bceb07a-e211-45d7-b74c-91845517b3fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 17,
                "y": 50
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "805824db-dea3-41ac-8209-45c64e815fa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 12,
                "y": 50
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "c6166dac-eec5-4a2f-bc39-c5f0ea916bd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 5,
                "y": 50
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "43436b5e-a332-4c85-985d-a45d943f543c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 14,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "125a78a8-5eda-4d5d-bbc7-c93fe83facbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 122,
                "y": 34
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "47de038b-967a-4969-a86f-7971c8c7151f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 117,
                "y": 34
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "e247d0a7-1333-4934-873c-a3e6540f18ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 40,
                "y": 50
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "4b325f18-093e-4b88-9f73-caac9bc1d6bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 112,
                "y": 34
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "cf677a56-6811-4c3e-8a7e-bcf58ef2baa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 102,
                "y": 34
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "841923e0-2a58-481e-94e1-3046ddb94532",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 97,
                "y": 34
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "bd5420e2-ec15-44b7-8ca2-c8218dc1d140",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 14,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 94,
                "y": 34
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "a0cc61cf-3dda-4e75-b8bd-9ae076b8e660",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 89,
                "y": 34
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "b17c2381-9052-404d-82ec-a8ef6715fbf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 83,
                "y": 34
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "391072e0-c3e0-4db9-9784-c1485d65e490",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 78,
                "y": 34
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "419d6b0b-11f3-4599-a49a-9c47401823fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 72,
                "y": 34
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "89967635-ede0-4bc5-8cee-150431f58681",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 66,
                "y": 34
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "1d3d03a9-41dd-412e-af48-b6c576b41414",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 60,
                "y": 34
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "a098373c-9761-4a26-9323-5e85c0f09452",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 106,
                "y": 34
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "cb903871-7433-4943-8fbf-8fe67faac52e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 45,
                "y": 50
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "1ba759f5-4e06-44b3-a762-0d05abf37f6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 51,
                "y": 50
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "7dce35a5-0820-4251-810b-07f215f08346",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 57,
                "y": 50
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "8d5f5b15-a094-465b-8305-8877e21a0c59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 64,
                "y": 66
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "2ea6d4f6-0f77-46e3-8a98-782844dfb73c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 14,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 61,
                "y": 66
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "b76aa77a-65d4-4558-850e-3d3a88afd69e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 57,
                "y": 66
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "374ade41-052d-490c-a8eb-d61d49d9dfb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 53,
                "y": 66
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "3e45b865-aa7c-4986-9fc2-5491f0b9a785",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 48,
                "y": 66
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "a8b753c2-90ae-40b0-9574-100f9875fecc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 44,
                "y": 66
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "f0e61fc0-e972-4091-9375-f1b1074e3e00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 38,
                "y": 66
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "6bc0d98c-6c98-42be-8169-3bbded99e867",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 30,
                "y": 66
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "1c4fa425-7ee2-4329-9891-469bc0279057",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 23,
                "y": 66
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "cd1a0b27-efc8-4bce-87c0-6b47d8c70be2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 16,
                "y": 66
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "b963070c-34e0-421b-a497-93832fb0bdaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 9,
                "y": 66
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "7a741208-e123-40dd-851c-c4a7d3fa8ce3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 66
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "64f994f8-3b53-47b3-8a9e-3bbcfa823be1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 116,
                "y": 50
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "25a56bbc-65ba-4e77-959a-579f28ad5f65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 110,
                "y": 50
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "c0201a11-877d-424c-be25-91e38f77a6ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 103,
                "y": 50
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "76912ccc-0c5c-4b91-aa7d-f8ce1481bceb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 96,
                "y": 50
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "ce8de8d6-459f-43a3-8b18-3b0669a8ec79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 91,
                "y": 50
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "ed770758-2706-43ee-954a-b4bb68b7bc2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 85,
                "y": 50
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "13c7d7bc-9d17-47f8-880d-51e6dec63b1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 78,
                "y": 50
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "15e850ed-216a-4966-bbba-1bfd788adfaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 71,
                "y": 50
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "25264ee0-ee72-4b43-838d-1029814191a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 63,
                "y": 50
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "3cfd270e-e6b6-4fa0-8724-4b96833838a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 53,
                "y": 34
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "f605c06a-875c-4339-bf76-6bfd31d887b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 46,
                "y": 34
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "28729f6a-6ca7-48b1-a097-1ecddc442002",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 39,
                "y": 34
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "c0ca884f-fe06-496e-9de5-ccd6fd4bb5d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 14,
                "y": 18
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "c2a92ad5-eb3c-4c01-8fa2-1445373c4a5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 18
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "bb83fd6d-5a26-49ef-942a-1c757b2dc735",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "960d29a1-f2e0-42a4-a90d-cdcf9ca8bef1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "d1e51fbe-576e-4bca-944c-1281f2ddbe29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "774f10f3-478e-46b6-8999-d5a2be6f78c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "02612b50-2e65-477e-8e73-3e9cc00170ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 88,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "c275a86c-3950-4f51-aad3-9c6c37d643ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "b7db6cf4-ca70-4e60-bae4-1e9aa853a243",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 76,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "428faa0f-eb50-4bff-a64d-34ce3e488c58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 69,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "5d9cf368-8528-4279-b616-41c21630f52b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 9,
                "y": 18
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "63d36f04-8b22-4bc9-924c-d4f01a42b820",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "5dfc71e7-c93e-402b-956a-344d358ed79b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "d18811d3-0de3-4818-97f0-1cfe661b2369",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "04597193-c6eb-4ded-8959-d8545b9af627",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "a97ed54b-3033-4dde-b8b6-390becab47b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "7ff6f78b-5061-4837-aa9f-3efe13ddd5c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "349d19e2-cc9c-4551-a10d-58d00c7ce973",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "947aa632-6b71-406a-b281-474942b8fdff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "49f27f0b-a3d3-4943-bd56-fbc13c117fb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 15,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "dbb97171-08f3-40db-b432-9cfb04c34ac5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "2b5713a7-40a5-4f12-b3ca-bb151d661821",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "9634cc1b-7584-49e3-acf2-244f48f0049d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 21,
                "y": 18
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "a3adf760-0432-4271-94fe-176a1137c682",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 84,
                "y": 18
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "404a5c9a-1641-4624-a8d4-777cc0a12355",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 27,
                "y": 18
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "a565b7a9-352e-4253-a93f-8961c79c3932",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 28,
                "y": 34
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "25001ff3-9b8d-4784-a229-cb7158bbd99d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 22,
                "y": 34
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "739691d1-8b0e-4e3f-aa08-110f03ee4a6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 17,
                "y": 34
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "97a7dd5e-d991-45e5-bd97-83dd9d6bea36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 9,
                "y": 34
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "84c7663a-4553-42e9-9369-6848a89ad823",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 34
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "88d0f76a-9e32-4c12-83fe-d9d7430b87fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 115,
                "y": 18
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "df9df8af-4f46-44a5-b7f2-10238fa45ae8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 108,
                "y": 18
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "564c089c-4181-4222-8ccd-b811190e98b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 102,
                "y": 18
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "c572cbe5-31fe-4369-a89c-86350d4fa9b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 95,
                "y": 18
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "8a26ffaa-4920-4cbc-879e-80da88831dda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 33,
                "y": 34
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "38a2bc6c-fcab-48d0-8040-cb4036eaa500",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 90,
                "y": 18
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "9fd6c5e0-631f-4da2-b447-8969bb4bebcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 78,
                "y": 18
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "44dfe33a-8ecf-4293-8f59-8121f07748c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 71,
                "y": 18
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "84747b1e-f99f-41cf-8a0d-f569742a88ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 63,
                "y": 18
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "90904873-a8f4-44b4-ae25-9024c3b51a34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 57,
                "y": 18
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "665cb658-f18d-498d-a2e6-84d065b7042d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 51,
                "y": 18
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "84e10ff8-7d2b-4969-93fd-eebf508bff63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 45,
                "y": 18
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "5e2bbbee-f639-412b-b546-3be255eb0949",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 40,
                "y": 18
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "97cc3c12-a71c-4b89-af7c-13e750ea79b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 14,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 37,
                "y": 18
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "88e6af41-7da5-4ee5-b345-1d8e98a6f3ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 32,
                "y": 18
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "b9d7707e-7eab-4318-9148-56f60db95bbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 70,
                "y": 66
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "1cc23b9d-537b-4314-9f46-3d3fede67fbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 76,
                "y": 66
            }
        }
    ],
    "hinting": 2,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 2,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\n0123456789 .,<>\"'&!?\\nthe quick brown fox jumps over the lazy dog\\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\nDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Medium",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}