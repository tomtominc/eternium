{
    "id": "f3f2c076-7226-464f-8382-4fee897560a5",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_pinch",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "ChevyRay - Pinch",
    "glyphOperations": 3,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "98cadc12-5e31-482a-8aff-9c3f15d74d9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 7,
                "offset": 0,
                "shift": 1,
                "w": 1,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "3fd32f6c-2ed0-409f-8954-2fd92e7d7d0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 7,
                "offset": 0,
                "shift": 1,
                "w": 1,
                "x": 35,
                "y": 29
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "02e9c630-52da-4a0c-9956-d3a16b78ba5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 7,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 30,
                "y": 29
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "03a03e4b-4f78-4b94-bc4d-27b33d7da598",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 23,
                "y": 29
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "e018fd1a-72d7-46e6-be21-5050be782d22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 16,
                "y": 29
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "22f6f9d9-f3a9-4d74-981b-2c0ca4aca8be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 9,
                "y": 29
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "9d3be526-0a3f-442b-8837-75555bf7065c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 29
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "846626bd-9e07-454a-a391-b3adb1e083da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 7,
                "offset": 0,
                "shift": 1,
                "w": 1,
                "x": 119,
                "y": 20
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "0220f92f-1da2-492c-9e35-7ffe6f746dfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 7,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 115,
                "y": 20
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "9596f3e1-e27b-4d7a-b328-e200ff3f2ffa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 7,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 111,
                "y": 20
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "e3eb722e-7cac-4492-af72-b8daa760ab83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 7,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 38,
                "y": 29
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "0da56714-a446-4212-83b4-83e5751f2f7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 104,
                "y": 20
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "d01c96c1-5379-402c-9f5b-0cae8127e324",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 7,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 94,
                "y": 20
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "475ad28d-701d-493f-b27a-3019c6a4f059",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 88,
                "y": 20
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "c7779c7b-210b-43a0-a796-f4d4cbcb1ea4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 7,
                "offset": 0,
                "shift": 1,
                "w": 1,
                "x": 85,
                "y": 20
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "aee0cd0d-130d-4ba0-aece-f64c145fbb70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 78,
                "y": 20
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "2e3c3835-099b-47d6-9d5a-4abedaa5fc09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 72,
                "y": 20
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "6532d4a2-58e0-4f31-ac95-37068d636f91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 7,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 67,
                "y": 20
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "1bbed15b-1758-45ae-879d-560dc0dbfdb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 61,
                "y": 20
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "c06a71c1-8a23-4b8c-a537-4bde4e1ebd12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 55,
                "y": 20
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "2bfd4d05-fdc1-44cf-b53c-0cf61f48612e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 49,
                "y": 20
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "67214b2c-b479-4e82-a6c8-ef3de95378e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 98,
                "y": 20
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "a8eff4b7-4c86-4aea-a74c-659215bcf592",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 43,
                "y": 29
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "26efd03d-05f2-42d4-8d55-fcea38d281c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 49,
                "y": 29
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "caf175bb-2839-41f2-8615-2c8fa7549fff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 55,
                "y": 29
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "4f8f4fcc-09fd-451a-9727-2c8f89bca71b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 59,
                "y": 38
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "b7adec73-79e4-4ed0-bde8-e8d829a75940",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 7,
                "offset": 0,
                "shift": 1,
                "w": 1,
                "x": 56,
                "y": 38
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "193aed25-1a6a-4655-8281-b52cf7ba8794",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 7,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 52,
                "y": 38
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "e8734e9a-f4db-41e2-a5fb-2d1a3e2b15c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 7,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 47,
                "y": 38
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "43b3c012-24f1-4324-90be-39ba2355d5b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 41,
                "y": 38
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "3c52564f-10a4-4e6a-986d-79db1286ea7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 7,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 36,
                "y": 38
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "57fae607-9df1-497e-8a88-8685e5e68f4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 30,
                "y": 38
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "a93e5439-a65e-4f95-bd7d-de5c96e953f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 23,
                "y": 38
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "fde491a4-3a33-44c8-bdfe-e36db7200f6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 16,
                "y": 38
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "f17d2132-c138-45bc-9184-2c6ff4dd1fee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 9,
                "y": 38
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "2479c0cd-8baf-4ac9-9750-e6c399e4c053",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 38
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "f9df2197-b787-49f5-964f-29697e9b095c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 120,
                "y": 29
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "03a17966-b95d-49cd-9839-3eceba8d85e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 114,
                "y": 29
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "27c16f5d-a5cf-40c0-aaf7-c8e2df892853",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 108,
                "y": 29
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "6e67fefc-7409-46d0-bd40-d1007c35a5c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 101,
                "y": 29
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "18b1454a-bc0c-4b63-af58-84f6aa42ab65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 94,
                "y": 29
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "6004f00d-4f82-4406-a2a0-ffd4f72b4dd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 87,
                "y": 29
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "31be9557-d81e-4485-9b6d-519eacda17fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 80,
                "y": 29
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "f25f73eb-2aa2-4a0d-8e3d-ebb5de8032f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 74,
                "y": 29
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "e82d2b89-67b4-433e-be28-8019ebc9f72f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 68,
                "y": 29
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "1fd23ea0-1731-4a86-bcf7-ca7786070b4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 61,
                "y": 29
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "ce224df9-2f6b-413f-87e6-ac8f1f9d1103",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 42,
                "y": 20
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "15366d0d-b51b-449b-9725-e669540754ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 35,
                "y": 20
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "22cf74ef-4bb3-4220-9e90-639437dcf2f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 28,
                "y": 20
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "5d1322f8-c32e-43d0-b557-7d8fe7cec464",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 13,
                "y": 11
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "a83e3ea0-2c5f-4024-8da5-8b94ea620f3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 11
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "879c104d-be5d-44f1-aade-51d78f037e9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "40789754-b3a7-4904-8592-f4a01d4f1f43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "a17b875c-2d70-4f12-9ede-8ed79cd10d05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "5bdcca5b-4954-470a-b338-ab5bfcb70225",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "2afcae38-5589-4220-af78-735e47d0255c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "9786c8ff-0ad3-44e3-aa1f-4b2e959d5240",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "bfe35964-9467-4087-9818-dcf8adebcd70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "ad41d62f-eff8-4620-8dd5-70806b644442",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "0f9f41c3-f103-4980-ade4-8a9e90aa38eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 7,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 9,
                "y": 11
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "cf6d969a-e29f-4368-bd5c-f2ae96f28141",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 59,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "49d133a0-2f91-4b93-aba3-0a110f19426a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 7,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "0292a51d-fd78-438c-875a-5f1f8d8e9b2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "9793081d-2bf1-4b69-a59e-3048cd9bca2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "3a7ca618-ca43-4e36-8967-b04203432c60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 7,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "9fc10631-916f-4daa-b72d-11383d27b11b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "ced13013-b4d7-43d4-ba62-f54f64948da8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "a52d4bfb-511b-4b0c-9c01-c8c4de544ba9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 7,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 16,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "8b4888a1-fba1-4356-9441-74ca13247c67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 10,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "7b80b689-0847-4cbc-806f-2869cea3dfc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 7,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 5,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "1aabb8a6-f8c2-4e72-b62a-7497176ede38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 7,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "b82126da-d8c2-404b-979d-85bde3e10a6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 20,
                "y": 11
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "c0611f1f-c546-4f5d-a213-a81bc7ac8140",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 80,
                "y": 11
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "560a77eb-d75b-4ec8-8d98-452c778bca72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 7,
                "offset": 0,
                "shift": 1,
                "w": 1,
                "x": 26,
                "y": 11
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "c6c77900-cb68-43e2-b475-ab1cec743cde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 7,
                "offset": -2,
                "shift": 1,
                "w": 3,
                "x": 18,
                "y": 20
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "d00018fd-cf35-49a4-b36d-a24c4cec9995",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 12,
                "y": 20
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "28ac3813-2d5b-4ca8-9d9b-e278974eecad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 7,
                "offset": 0,
                "shift": 1,
                "w": 1,
                "x": 9,
                "y": 20
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "98dea675-d79a-4420-bc61-5c3c05bfeae5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 20
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "17c1da35-4445-4929-af00-91170614284b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 115,
                "y": 11
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "d4e3aaa0-8d37-4f68-b01f-6bd0b1053082",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 109,
                "y": 11
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "5632ac8b-49c8-4155-87d1-03aaa29d0571",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 103,
                "y": 11
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "b385d5b1-2ae0-474e-8a85-6067f35b2c88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 96,
                "y": 11
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "08735d14-58da-40a9-a291-bfef8bbaff80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 7,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 91,
                "y": 11
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "2fa55dd8-1fb3-4a3d-af23-71a63a4c067f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 7,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 23,
                "y": 20
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "2f31ba48-639f-435d-9166-1efa8511b08b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 7,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 86,
                "y": 11
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "60c20a4b-1f19-425f-a7bf-16ac0671a6c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 74,
                "y": 11
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "7b8264f9-3207-4e53-be21-9262c7cb1186",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 67,
                "y": 11
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "6d4e8db9-d91e-460f-afcc-a25524fc96bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 60,
                "y": 11
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "34c2f307-51d0-459e-a342-5fd1485c6090",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 54,
                "y": 11
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "99ba6d3c-d947-40ba-8f80-f36cba467571",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 48,
                "y": 11
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "1b8d63c9-246d-4380-bf9d-21cb7fecaa1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 42,
                "y": 11
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "871602f9-719b-4a0c-8ef1-b38c4972455d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 7,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 37,
                "y": 11
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "6e44dd1d-d827-4302-bc97-f0d5fc0097dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 7,
                "offset": 0,
                "shift": 1,
                "w": 1,
                "x": 34,
                "y": 11
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "e220621a-17c5-4c4e-a481-46a446eb5a76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 7,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 29,
                "y": 11
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "323c6b7d-6078-4824-9108-24f06c94bc9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 65,
                "y": 38
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "99fd3934-0602-494d-9f8a-bacafd75cb96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 7,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 71,
                "y": 38
            }
        }
    ],
    "hinting": 2,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "9c3e8654-f951-468b-b5d4-3613a757d0ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 106
        },
        {
            "id": "0cfc084d-c93c-472c-a4bb-1824b810aade",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 106
        },
        {
            "id": "8516bcc8-02e7-4b99-9fa6-be0900bbd74e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 106
        },
        {
            "id": "b9ce66c4-3867-4beb-805e-1257df96cdc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 106
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\n0123456789 .,<>\"'&!?\\nthe quick brown fox jumps over the lazy dog\\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\nDefault character: ▯ (9647)",
    "size": 5,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}