{
    "id": "f3f2c076-7226-464f-8382-4fee897560a5",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_pinch",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "ChevyRay - Pinch",
    "glyphOperations": 3,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "435f49a2-f558-4ed6-8cea-b15a0e58109d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 7,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "a8c196ad-d5ac-4e93-93af-f82808d0683b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 7,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 35,
                "y": 29
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "a3dfe73b-ed09-478d-a523-27817c098248",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 30,
                "y": 29
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "094ba659-963a-4a3e-b717-fb42abd6b0cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 23,
                "y": 29
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "fb543056-6336-4b84-8d5a-86fe1717fb9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 16,
                "y": 29
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "43cc3821-eaed-41eb-b704-3e6a8287f3e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 9,
                "y": 29
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "fd039b51-29b8-40d9-8e16-d2b89606bc4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 29
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "178c68c0-2c1c-4b72-9c6c-73ea4f3f605a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 7,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 119,
                "y": 20
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "026c1018-4588-40b9-b876-0a67d0feaac7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 7,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 115,
                "y": 20
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "428dd531-bb01-4f31-a6cc-22c44ab74386",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 7,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 111,
                "y": 20
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "4b50a9dc-aa0d-4b75-838f-3674dd2fb8c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 38,
                "y": 29
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "09976df3-ba3e-46a4-8445-e5993be317a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 104,
                "y": 20
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "9f9dd060-a577-4d30-a3cc-a7ca4370f139",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 7,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 94,
                "y": 20
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "86e15a35-3b26-4c61-b2c6-69e1af3a3f92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 88,
                "y": 20
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "ba2a3df3-6acd-4ec4-b77f-275382b0c785",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 7,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 85,
                "y": 20
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "adc6a27a-aa7a-4179-bddf-f67af98b4e5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 78,
                "y": 20
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "9d8c2fbf-7b02-4c5f-a86b-ffaee25a967c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 72,
                "y": 20
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "1bc3350f-9b66-4705-a6ed-1e362521afa0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 67,
                "y": 20
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "7fa0f39d-a8cb-49ee-b22f-f065b738fadc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 61,
                "y": 20
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "ad70f584-3237-4468-bdca-baf1f3ae14d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 55,
                "y": 20
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "d8999d4e-ecd3-4c40-9162-13860928bfde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 49,
                "y": 20
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "95a514e3-8f42-4511-81c3-3d43232194a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 98,
                "y": 20
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "800b00c5-6e6d-415a-8ba6-4c2f162e4c13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 43,
                "y": 29
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "d6544001-e9b7-47c6-8cd3-967419abd502",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 49,
                "y": 29
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "36022fd7-304e-4e9b-90d0-8230a861c935",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 55,
                "y": 29
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "4d53bf29-023c-4930-b130-6b409a43c52f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 59,
                "y": 38
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "49f86f03-31c1-4f8b-9504-84ce56de2f6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 7,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 56,
                "y": 38
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "55f86ede-b52b-4312-abda-7d68207abb39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 7,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 52,
                "y": 38
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "37d8ee5d-af6d-4222-b404-6adec740c7d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 47,
                "y": 38
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "a47d6c48-b034-46a0-b0d2-52c81c00220b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 41,
                "y": 38
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "5057c052-e131-42a2-b824-816ebb45122e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 36,
                "y": 38
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "37c9e114-6985-49bd-95be-e909fc8a1d32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 30,
                "y": 38
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "e68b26e3-5cb2-4406-8c5e-63a2795d646e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 23,
                "y": 38
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "0f684427-2e93-4b7b-9fa3-636036f4e057",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 16,
                "y": 38
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "dfb6d4bc-378a-4a36-8927-7ab3fd36cbed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 9,
                "y": 38
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "071f0ca8-82d0-49b6-8598-7826201a59b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 38
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "efeedac8-c30c-4448-ae71-a11dca6961ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 120,
                "y": 29
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "e0e49399-a527-4129-815b-0c46c953d68d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 114,
                "y": 29
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "2232ce0d-5bcf-4573-a289-36456b1ed296",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 108,
                "y": 29
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "3db68a84-cc31-4f05-9616-87b2b4f76424",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 101,
                "y": 29
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "d0b088b9-8e03-412d-9929-8dd1234312ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 94,
                "y": 29
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "c968d743-abb6-4e60-ad92-90ff91aac3ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 87,
                "y": 29
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "3cf5773f-53b3-4a42-ac9d-39ba0889e961",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 80,
                "y": 29
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "62a7b73e-2e29-4a12-925c-ca5e01cfb705",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 74,
                "y": 29
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "dfac4859-cc6c-45a2-a1c0-b17c59ef4062",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 68,
                "y": 29
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "79de76c3-7d47-45d5-912d-ebe3780508e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 61,
                "y": 29
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "713c0915-4b0c-4642-a917-ed7f9d8f0bbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 42,
                "y": 20
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "cfff05b6-387e-413a-b77f-b78c6ab22bca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 35,
                "y": 20
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "bc464864-6188-4354-935e-64cf1fdce419",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 28,
                "y": 20
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "f421bb16-de87-4293-b66f-1ad9547db13a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 13,
                "y": 11
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "29b72288-08d3-4ff5-88a5-ce8ac4d4e1b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 11
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "2d84235a-9e69-4ad8-94c1-cd804dd4d351",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "a4ec6bd2-bf43-4374-9880-3397989cc4b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "a1fb04bc-7c2f-4fb8-b454-40613f41d9a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "34ad4dce-2b63-4059-a6bc-a90a36cc57aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "ef70f9a7-0afd-4bd5-a9bb-38a1b0f17f76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 88,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "b00eb3d3-d795-4706-a28a-9103e7bb05c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "0e8f2412-a327-4edc-8109-bceb3648a1ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "b3dcf413-9aa4-4422-8c24-c84e537d16e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "fcf619e4-281f-4146-85a2-fc56ca3d7d6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 7,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 9,
                "y": 11
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "74b201bc-3b05-4889-940f-25f2605395b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "82702b32-09fc-47cb-b89a-6e22cc1ea874",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 7,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "8d214224-9914-4a19-b17b-2cd1b14a7bc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "a3e20be0-3a5b-409e-861f-62729e9c3757",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 38,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "7c41e664-2051-47c1-b7a3-088677898642",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 7,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "990c052b-d873-41b0-b06a-462f8a5dea9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 28,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "5f5bb2e0-a0d7-4ea0-aaf6-8ebd8632c09f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "d39f6fcc-b5bb-4bcb-97a4-36e97c5e9302",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "964556f2-bdf6-4a1b-93ea-0a8368409810",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 11,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "fe31ee54-9a34-4605-8692-a0bd96ff2a07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 6,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "a140c213-dfd2-49f6-b0bb-4ef0b41efbfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "fb1fd30a-93aa-4f3f-9060-765081479bec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 20,
                "y": 11
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "32056f91-cf61-4d1e-b41a-2b82d17b8d61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 80,
                "y": 11
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "3e6c2007-b6bd-409c-bdf2-f16ec0c3c6b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 7,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 26,
                "y": 11
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "1b06ccf1-2bfd-4a0e-a39f-69a3e478cf17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 7,
                "offset": -2,
                "shift": 2,
                "w": 3,
                "x": 18,
                "y": 20
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "58f5fe40-4f94-4287-841a-5ff36146fedc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 12,
                "y": 20
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "08524ca8-34cc-4a3a-9521-46656b480e34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 7,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 9,
                "y": 20
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "612b215f-f64f-40f5-99ad-76c22eba36f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 20
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "0e409fbf-12cd-4ff4-94a6-1db16dd7a05e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 115,
                "y": 11
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "2d386d30-7d9a-4da6-8fa5-298fff4956c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 109,
                "y": 11
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "ba1851a7-a5a9-4197-8d32-911b59e9aeb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 103,
                "y": 11
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "3a96a335-5c5d-4fb7-acc8-1083e35ee60d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 96,
                "y": 11
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "2fb19a34-c520-4c0d-be30-7ebf31145665",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 91,
                "y": 11
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "1c53fb70-24a4-4017-842e-6f919090cd67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 23,
                "y": 20
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "c3bfa417-3fb3-45bb-9fbc-533c0ad29d7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 86,
                "y": 11
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "5d9bfda5-f406-4774-a5ad-3c0c1f0d1339",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 74,
                "y": 11
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "84b09878-e8c9-472b-9bb2-aa563e636f7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 67,
                "y": 11
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "18fea2bf-644a-4f09-a7d5-10f506495401",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 7,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 60,
                "y": 11
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "f81df0c3-1d0d-4fb0-9d77-2d94786dc510",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 54,
                "y": 11
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "d7804b0f-6960-4c6b-881d-6d34799cfc1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 48,
                "y": 11
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "4ba7364a-98b3-439a-ab0d-9c3ef4c8861c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 42,
                "y": 11
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "931cc541-3b9b-4ccb-b4f6-39f7dc4fe69a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 37,
                "y": 11
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "e54bb89a-b684-4658-b299-59b2a3ab2005",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 7,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 34,
                "y": 11
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "eefaef06-c0e0-42d7-9bf3-e59c829c00b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 7,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 29,
                "y": 11
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "15aa0b9b-8c32-455f-b212-f536b9784312",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 7,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 65,
                "y": 38
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "b3d539cd-e456-4353-ba74-fd1894e338ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 7,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 71,
                "y": 38
            }
        }
    ],
    "hinting": 2,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "b8ae8c33-9dbb-4ae7-b667-be12bc3ee3d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 106
        },
        {
            "id": "2b1c5abf-7078-49ce-8fc1-bd4fa163d27f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 106
        },
        {
            "id": "dfe2855f-b8c8-4149-9cc5-9ec0da39d944",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 106
        },
        {
            "id": "01bcc3e2-79f1-4c6f-9050-ba6882ddda24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 106
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 2,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\n0123456789 .,<>\"'&!?\\nthe quick brown fox jumps over the lazy dog\\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\nDefault character: ▯ (9647)",
    "size": 5,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}