{
    "id": "06c7c204-77dd-4c71-a376-aedc562cc3cc",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_baby_blocks",
    "AntiAlias": 0,
    "TTFName": null,
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "ChevyRay - Babyblocks",
    "glyphOperations": 3,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "20b6260e-d889-42b4-b1c7-915253b266be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 11,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "d14a0203-d409-4c71-8a05-113eaaf83658",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 11,
                "offset": 0,
                "shift": 3,
                "w": 1,
                "x": 42,
                "y": 41
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "972230d0-4636-4988-9804-564b7c0997d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 36,
                "y": 41
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "0ec7a1af-b23a-40e6-b9f4-360eca7a77a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 11,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 27,
                "y": 41
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "1ce96a60-7633-4889-94e8-cbf19727c031",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 11,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 18,
                "y": 41
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "c01ff2e1-db50-4a44-aa64-58348d38e5ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 11,
                "y": 41
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "622c6669-434d-4e19-b1d6-9e3b8a7ea1cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 11,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 41
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "62648388-577d-4bd7-9450-6e3dd8673fef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 11,
                "offset": 0,
                "shift": 3,
                "w": 1,
                "x": 121,
                "y": 28
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "8b7e48aa-8203-45c5-9302-cb4415ea23a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 11,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 116,
                "y": 28
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "33e637e4-47a7-4d92-8eaa-0cad213a3383",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 11,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 111,
                "y": 28
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "faa161ae-819c-4352-82f9-6d9f82b8ef08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 45,
                "y": 41
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "eb3fff4b-e01d-465f-adbb-847825417ef8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 105,
                "y": 28
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "e3a87ae7-f7b0-470c-a256-64b6237031e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 11,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 94,
                "y": 28
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "522ff2f1-ac86-4351-9270-9eddeb6e9b41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 88,
                "y": 28
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "4d9a5f70-44c3-4c16-a932-62c67af7f2cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 11,
                "offset": 0,
                "shift": 3,
                "w": 1,
                "x": 85,
                "y": 28
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "e09510f3-c83e-441e-806d-fd58dfc6098d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 78,
                "y": 28
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "46fdda9e-73fc-470d-af50-7ddc5c44fea5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 72,
                "y": 28
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "75c3a384-6bce-4fa8-8015-56af6c9cd67b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 66,
                "y": 28
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "930c9b7d-40bf-4c1c-bbf7-ae1184348b0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 60,
                "y": 28
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "25635acf-74d3-4d50-8f90-3d408bb3a253",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 54,
                "y": 28
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "73d3061a-f17b-4145-b4dd-61b610f8c908",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 48,
                "y": 28
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "cf5483a1-e11e-4390-9fc5-e4c4cf804236",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 99,
                "y": 28
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "f266cb0e-2249-407b-9104-9ac2fbff4093",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 51,
                "y": 41
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "54b1f2e7-70d9-42bf-b673-fa3e5dd24eef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 57,
                "y": 41
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "ceb9dc30-5c04-4175-8ac0-70f964f09b86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 63,
                "y": 41
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "904bd2bb-1456-4fa8-9d02-abbbc49ca1db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 65,
                "y": 54
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "4c6e72b0-e9fa-42f9-98bd-25dd0239ee7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 11,
                "offset": 0,
                "shift": 3,
                "w": 1,
                "x": 62,
                "y": 54
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "cae3a789-7861-4abf-aecb-8b2006e85087",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 11,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 57,
                "y": 54
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "e6950d14-d443-442e-9c63-4cfee7f4a6b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 51,
                "y": 54
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "6f103a68-ef73-4306-8daf-e126b64fc7f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 45,
                "y": 54
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "33df0b64-70ed-462d-a6e0-61ebba10cf1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 39,
                "y": 54
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "b5800ade-1f27-49b5-957d-116bec3ab69d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 33,
                "y": 54
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "e40fadae-73e6-46f4-812a-4dc20f3ca78c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 26,
                "y": 54
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "bbfee43d-6b29-4a19-994f-d5c100dd648a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 20,
                "y": 54
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "a076090e-9449-4504-87d3-e57fa7088cef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 14,
                "y": 54
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "843ffb95-3604-47c7-8946-d3070ca2446b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 8,
                "y": 54
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "fa8a9de1-ae78-4ea1-9486-737d5cdaf4ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 2,
                "y": 54
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "61c4f02e-901f-4bc9-8013-4316f43b062c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 117,
                "y": 41
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "10cfa17c-f29b-46d0-b4ac-f02e8510d30d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 111,
                "y": 41
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "8220ae1b-cd63-457a-b520-29d7587a9b18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 105,
                "y": 41
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "97820817-2501-4a03-9f0b-3d842b3216a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 99,
                "y": 41
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "bf753cd4-8771-47f5-ba6c-13b0c5f92f5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 11,
                "offset": 0,
                "shift": 3,
                "w": 1,
                "x": 96,
                "y": 41
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "0a5bbc5f-7120-44ab-81c2-f829e60c4491",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 90,
                "y": 41
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "b3729090-e5e5-4ebd-8cf2-64f207e4171a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 84,
                "y": 41
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "b88561ae-e932-464c-8a16-61c2a4cc8279",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 78,
                "y": 41
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "3578d35d-80b3-4578-b3c1-0cb70fa30dd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 11,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 69,
                "y": 41
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "6bc9ebca-47a9-49ff-8925-bec408ab8751",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 41,
                "y": 28
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "d3a8d37c-9e9d-40fb-bdf1-880a702ddb6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 35,
                "y": 28
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "ffd93878-ad05-4cb9-a4fc-c76956f1e5a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 29,
                "y": 28
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "e0c7df4f-9785-44cf-8e9d-ebec44ccc796",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 13,
                "y": 15
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "d8d75b85-96f4-4e17-8da1-ee01ec2fdffd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 2,
                "y": 15
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "4a02d705-a53b-4ede-a113-b7765872be61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 118,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "b6a5354e-4af6-4e8d-8261-9dcda979b2e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "c918254d-c022-4be4-b188-a2cecd67c92b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "e8158d0f-6eb5-4621-9999-42b18f9915c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "2a7c0aea-8ab4-4a60-9acf-73ba0eb224db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 11,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "6ab843ad-9bd5-403c-87f2-1e768a912ea6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 85,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "e3c55573-b9e0-4d65-8b10-8ceaf2ce22c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "5fa4ae54-9fbd-4135-91bd-2fb498c82b72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "2e4232cc-6ba7-4999-bcc5-d409d984615c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 11,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 8,
                "y": 15
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "7bd58e36-f9cd-408a-899e-bdd2daecc8ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "1b8c0a46-bd9c-487d-a292-bd825b393c83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 11,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "b1f1e6de-18df-4b97-9b45-0282891cb973",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "73099911-121b-4361-a07e-dfeaf7baaa84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "1ca11e3e-0e7a-430c-b3ff-792de9e286d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 11,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "56599114-0e27-44e6-abe4-9ce4df8a84bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "d84ed050-4552-4888-beda-a21532336fa9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "585b3efd-8309-4beb-b176-dfafddb1ce3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "07ad7eff-eaad-41e4-aa60-f2b5995ec697",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "37e3b9a9-6b92-449f-838d-00949a0c0e00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "d8ad7a0e-57da-4fb0-9f84-e3415d05d0d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 11,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "22227dc4-c2b7-4819-b55f-889afab063c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 19,
                "y": 15
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "e5eba6a5-b552-4221-b42e-6b3b980a998c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 81,
                "y": 15
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "010a70c3-2202-4205-9851-6f6683846dd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 11,
                "offset": 0,
                "shift": 3,
                "w": 1,
                "x": 25,
                "y": 15
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "b324765d-e647-4fe7-838f-918ebc2b50f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 11,
                "offset": -1,
                "shift": 3,
                "w": 2,
                "x": 20,
                "y": 28
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "a4adab5a-34a1-466f-9e7f-ba04a0ba74db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 14,
                "y": 28
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "97ff8700-3f88-4469-870d-530bdc3d31a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 11,
                "offset": 0,
                "shift": 3,
                "w": 1,
                "x": 11,
                "y": 28
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "6419a9f4-1d48-46a9-bac9-374117935546",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 11,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 28
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "347ee4cc-1e12-4fc5-808e-6b627b816052",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 118,
                "y": 15
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "c46206da-a17b-4e34-a4b7-7adf46336e9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 112,
                "y": 15
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "1fc79d17-a016-4ee5-970e-2aaccbae977e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 106,
                "y": 15
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "29325d70-36a9-4412-bce7-cb1c2db240a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 99,
                "y": 15
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "4b9173a8-36c7-437e-bc7f-7eb6d06a069a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 93,
                "y": 15
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "b20c2f68-429b-4273-93de-2677ca931d40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 11,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 24,
                "y": 28
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "ae67596d-7633-4a96-8bff-e3118cf9431b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 87,
                "y": 15
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "272776f1-7b58-4ff2-98f5-6ac546548a5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 75,
                "y": 15
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "5daf2ec8-00bf-494b-86fa-993531e0471b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 69,
                "y": 15
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "88b8e0bf-3edf-473d-a640-6ae867e4d3d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 11,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 60,
                "y": 15
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "40375e4c-f0cb-486c-a736-aa8f0891fc15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 54,
                "y": 15
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "2932ade5-340f-46ab-957c-1d1f31ab5116",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 48,
                "y": 15
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "17235b4b-1c7a-4b5a-a4d1-2c2ea4d762e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 11,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 43,
                "y": 15
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "555f433d-7da1-4882-98b2-ae6a902731c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 37,
                "y": 15
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "8692640d-5c5e-4689-ace8-01b0a2394f32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 11,
                "offset": 0,
                "shift": 3,
                "w": 1,
                "x": 34,
                "y": 15
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "4c398db7-adc7-4dcc-9942-b4c42c1d49da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 28,
                "y": 15
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "416bf6a2-2da7-4990-9e55-c0505b346a0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 71,
                "y": 54
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "21a6b8e2-aac6-498e-8e97-0b9a26306b94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 11,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 78,
                "y": 54
            }
        }
    ],
    "hinting": 2,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "f1130f84-16f8-4baa-8631-88a3bac8d6c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 52
        },
        {
            "id": "d8bb7647-f72d-4c09-8d7f-4200ade7a2ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 55
        },
        {
            "id": "879950e7-53bb-481d-85bd-d460031455ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 57
        },
        {
            "id": "b6fced7b-a5b0-48a7-b676-1eebb56d089d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 55
        },
        {
            "id": "77d9e297-5f55-4ff0-84ad-196b06cbd413",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 84
        },
        {
            "id": "48891a09-a5e7-48cb-ae4a-1da7424936d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 55
        },
        {
            "id": "a61f4015-a9cc-4167-9ac5-0d471d17c037",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 57
        },
        {
            "id": "a059f6eb-3144-42b3-9423-714719728d08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "eeb13370-3995-43b2-ab2a-45ef72aa937a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "0cf298ea-21c8-45c8-981d-856417894d06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 118
        },
        {
            "id": "a8afe188-c2e3-445f-b4af-66591cf0e344",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 119
        },
        {
            "id": "12787daa-2628-484a-abb0-cc6392bbb4d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "1fdb4787-454a-4f16-9312-24aee0c18d2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "bd1a83d0-51af-4565-9c97-863fa95f350e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 100
        },
        {
            "id": "413dd8a9-252b-4f1a-a8f9-c625ecc501e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "a5a0b74c-b3da-4cbc-9363-42dbe6071900",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 103
        },
        {
            "id": "2c9f4077-9ebf-41a4-93c5-ac2de03226c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 109
        },
        {
            "id": "cfbd4aa8-a1c6-46c4-af39-49ede6b554ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 110
        },
        {
            "id": "230a56a6-88f5-459c-b9ad-b91c5fc42072",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "0fd3ddaa-9e40-42a3-98ce-e955362600be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 112
        },
        {
            "id": "8678d207-4ec0-40e5-882c-52d383ca411d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 113
        },
        {
            "id": "11651bff-e047-4fd7-a1e7-c539ca5532e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "8b2e6341-2ce3-422e-a642-b2cee4cb988d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "4d53be3b-f131-4495-8f0d-acf866e70616",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "0a78230f-32e9-4b90-97fc-40eecea3667b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 118
        },
        {
            "id": "83bbfd47-082c-4982-bf90-e4f4863bcc2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "5025de78-4959-4683-b6c9-34eb2978cd32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 120
        },
        {
            "id": "557c95ec-e307-416d-9594-7c6fad3566e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "879d9b4a-a283-4222-a33e-bc4c834c8e93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 122
        },
        {
            "id": "92d29824-915c-4817-b74e-ae35ca6c4c25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 84
        },
        {
            "id": "63c1c48b-00e5-4aa6-b968-c3640b365edc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 85
        },
        {
            "id": "833dcd74-f775-4ba6-9e61-ab8da2b4134c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 86
        },
        {
            "id": "08aef652-c489-4f80-a9f2-d226ad5bd8fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 111
        },
        {
            "id": "4e3b4aed-ea34-4cba-958b-ec55cc7d38dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 118
        },
        {
            "id": "9ba2e707-c424-41c5-a24f-69e94b0ab423",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 84
        },
        {
            "id": "5d51004f-3da8-4133-ad73-5674bb30feaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 84
        },
        {
            "id": "89ebc4ba-2a09-4403-b99d-258012340681",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 84
        },
        {
            "id": "25333972-e275-4a3f-8dd4-decdcc1b446f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 84
        },
        {
            "id": "1916de83-f5c7-4245-afd1-f6a48d5b0c9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 84
        },
        {
            "id": "0c24f805-e467-49a3-aa88-eea70ebc6e92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 84
        },
        {
            "id": "dd9ab2df-df2e-4549-ab55-827a22d2c4f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 84
        },
        {
            "id": "96f3b64e-d268-4a0a-a7f3-6c4bd437918c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 84
        },
        {
            "id": "d823572b-c666-450b-a8a9-a467adb4a883",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 84
        },
        {
            "id": "d1ba1b31-0cb7-4da0-9b8f-918e53e8cc3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 84
        },
        {
            "id": "80e181ef-a678-4596-9cbe-0bf0c009da32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 84
        },
        {
            "id": "d7ff0aaa-fec1-4911-b5a8-7a63db16ef63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 106
        },
        {
            "id": "214aa2b5-7b25-427e-8aa2-458ea7cd8737",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 84
        },
        {
            "id": "f9fd13ad-0d99-4b60-a5e7-e8d5b9403dda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 84
        },
        {
            "id": "346ab392-4567-4ae3-ae93-c507f8b8e3f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 84
        },
        {
            "id": "88b9667e-4193-4ce0-ab8d-65862cb690ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 84
        },
        {
            "id": "f64b132c-849d-4e72-a8b4-d54a598bc5e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 84
        },
        {
            "id": "cbcb6a73-3760-4a2c-9956-acd00c2ef7db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 84
        },
        {
            "id": "721e6167-8533-45c6-81cb-e1f18fc8262c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 84
        },
        {
            "id": "d060d810-0bfc-413e-8ef5-19191703c5e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 84
        },
        {
            "id": "1963f1c7-762a-414e-8e9d-9149361a474f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 84
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 2,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\n0123456789 .,<>\"'&!?\\nthe quick brown fox jumps over the lazy dog\\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\nDefault character: ▯ (9647)",
    "size": 8,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}