{
    "id": "06c7c204-77dd-4c71-a376-aedc562cc3cc",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font1",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Kirby Classic",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "d218760a-f714-4ae4-99d5-7de2a02140c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "62d6f814-dc5d-4778-980a-aac9c9c0879b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 20,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 181,
                "y": 46
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "320d8148-f3f9-4c98-bdc1-6cf5328165e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 20,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 173,
                "y": 46
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "7cc25fe2-3f88-442b-8739-0c6e6552166e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 161,
                "y": 46
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "e94e8499-38de-4ac3-8737-10d97c2cb0f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 151,
                "y": 46
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "55a70263-a0d5-467e-b26e-5f6585a4d15d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 139,
                "y": 46
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "a48009d6-d35b-4b07-aaf7-50a5def58d7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 127,
                "y": 46
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "8bf58972-522d-4b2e-b222-b7bdb61eeaf8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 121,
                "y": 46
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "219771de-fd10-4684-86ad-a0aa4c44e356",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 20,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 113,
                "y": 46
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "e7e29ca2-5f2c-4d1a-a384-c7fbafa8710b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 20,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 105,
                "y": 46
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "8cf723c6-6167-4039-8064-62ab8a34b15b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 186,
                "y": 46
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "07172ce7-d57c-4d95-85ae-f0de7c5867d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 95,
                "y": 46
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "d6e03383-7d63-4f8e-9e2f-618f7950585e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 20,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 78,
                "y": 46
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "2b17bece-ac31-4272-bfac-c0d32dc5431b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 20,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 70,
                "y": 46
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "c25eb98b-072b-4904-a88d-988233256240",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 20,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 65,
                "y": 46
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "f24126ac-8ee3-4f1c-8c19-0a42b9fccf1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 56,
                "y": 46
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "f9bd414c-1b46-4830-a4fc-4ed11fbed371",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 45,
                "y": 46
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "d292f2cb-67dc-4583-8f6c-40fd40ed0d74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 37,
                "y": 46
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "49fd890a-54cc-4763-bbe4-0b29b70828b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 20,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 25,
                "y": 46
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "e0c1d703-c763-4acc-a4f8-30678d44071b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 14,
                "y": 46
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "396d2dcd-6828-4543-a16e-32d3930a6c88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 46
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "611b4839-97c8-4d5f-b086-29929d080174",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 84,
                "y": 46
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "4e22cb1e-e764-4fdf-86b1-d3e66765f042",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 196,
                "y": 46
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "7407b93b-30c7-4f12-8eec-a62a28dec2b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 207,
                "y": 46
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "6e0b562d-3536-4892-9418-ab4646fc3ada",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 219,
                "y": 46
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "619dccd7-320f-4750-b55a-165c04aaf67c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 179,
                "y": 68
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "50347e05-34aa-4271-a6b6-06faa048ce6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 20,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 174,
                "y": 68
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "da51b19b-5683-4aaa-9ca5-f80e881a83d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 20,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 169,
                "y": 68
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "21749102-bac4-4568-acb3-42e1e486c6c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 159,
                "y": 68
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "650da115-1d3a-4fd3-843c-55245a9e4d34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 149,
                "y": 68
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "bc6644af-abcd-4ac5-b8d5-0e33f2d00ba0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 139,
                "y": 68
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "5cc7715a-ed86-49f0-afbe-5e4e67b6126a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 129,
                "y": 68
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "c7a59811-de00-4840-941c-1243512214b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 117,
                "y": 68
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "eb8aa69f-e572-476b-9458-3a1e168ae20d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 105,
                "y": 68
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "02676cd7-ad03-42fd-a61e-353b0faf2171",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 20,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 93,
                "y": 68
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "48a2e541-30c1-4247-b7bc-ada7610b15c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 82,
                "y": 68
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "76ca6ff8-cb30-4e2a-921c-bf71254a45aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 70,
                "y": 68
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "05c9d095-be03-47a4-86f0-d6a440f6db15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 60,
                "y": 68
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "efa512fc-4b8a-43f0-b448-54fafbcbef4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 50,
                "y": 68
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "ca5fc8c0-2043-4a24-ad58-d42b8612a0c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 39,
                "y": 68
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "ffbe77fa-ecd4-4382-97d4-65db21b5c300",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 27,
                "y": 68
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "a4045df9-1c8c-41df-b9c6-d93add74ac10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 20,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 22,
                "y": 68
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "18d8f977-d283-4721-8d12-f325cf36dff9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 12,
                "y": 68
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "197f1f6b-d45f-493d-8a12-29a41e589c50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 68
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "d9d23ca8-6056-4c8a-8179-d7648710738f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 242,
                "y": 46
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "75b27e8d-46d6-4203-8ad6-a7d6d18e28f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 20,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 230,
                "y": 46
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "3f965a37-bb3d-445f-b2a0-15d066645020",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 244,
                "y": 24
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "12080a05-6e94-44ae-a6ef-071de46113ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 233,
                "y": 24
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "a659ffce-f154-40b5-8d53-c92e2e4cc870",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 221,
                "y": 24
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "e2e3df99-03bb-448b-8259-6fbc2ece1c02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "987c294c-9833-4e47-9fa1-3575432e061a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "8eaca41a-b9f8-4fd4-aa1d-51445b829cc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 20,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 200,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "d964d2e5-2b1c-4556-91e2-18decace3dcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 190,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "f34767d7-73f0-41bd-b56a-2224d0f4ffd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 181,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "67662198-953d-41a6-a4ea-e0bdf468db72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 20,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 169,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "af08f7c6-f1e9-4b05-85a4-b05b1aed7c35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 157,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "f5a5fcae-9107-4492-8aa9-e7f8e9aa92ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 20,
                "offset": -1,
                "shift": 8,
                "w": 9,
                "x": 146,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "1653f084-3a7e-4418-a94e-35deae3a1c41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 20,
                "offset": -1,
                "shift": 8,
                "w": 9,
                "x": 135,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "69de0aa5-bc7d-49c4-bb05-271511aad6e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 125,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "34e86b7f-e7ac-4445-9fae-bde09e1d89c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 20,
                "offset": 3,
                "shift": 10,
                "w": 5,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "bcf92a70-4305-4fdf-850a-532f149df209",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "f6fcea7d-aa81-452e-9cf0-581d8ee0d0f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 20,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 99,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "db840bb6-6fd5-4436-866e-c479457318b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 88,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "dfafe1e6-43aa-4c70-9384-845e286120d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 20,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "aab8e150-c225-407c-971f-52e4b3342a35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 20,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 69,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "e0f6d391-c26b-47d7-8687-8a1eb5468f25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 20,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 57,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "11f9107a-03c6-44b4-9338-98448653f0b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 20,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "c9f199b3-40b9-49ab-93cd-1982a2dbc6bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 20,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "58636329-b719-4ccd-a81a-ac470078a29f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 20,
                "offset": -1,
                "shift": 9,
                "w": 9,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "8b70af23-3f38-4653-b91b-72861572a941",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 12,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "9a699c8c-5a99-496a-9c36-181fe38d1c3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "14ed10c1-1cf8-4449-9fc0-30c582f616cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 240,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "ab120a14-ec09-4d29-9652-31d102f6ddc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 20,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 93,
                "y": 24
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "a0632269-c017-464c-8ec4-99985d5d0b76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 24
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "292c2332-8895-4eea-9929-e1f8087c39a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 202,
                "y": 24
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "f5ffdccd-e518-4dee-b012-8e4a2d41f9a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 192,
                "y": 24
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "075bb845-a00e-414f-a2af-4c3d3e2a9062",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 183,
                "y": 24
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "f4b2069f-6663-49dd-8fc8-633f57d69153",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 20,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 171,
                "y": 24
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "82ead04e-6223-4ef8-bc42-332fe7046da3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 161,
                "y": 24
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "787571ed-3634-48b5-a11f-e3edc35a9e2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 150,
                "y": 24
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "9ab88dd9-b5ad-43e5-8a90-c257e2468c91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 20,
                "offset": -1,
                "shift": 8,
                "w": 9,
                "x": 139,
                "y": 24
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "29e00725-b8b5-4299-80b1-35b0015f320a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 128,
                "y": 24
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "24e38bc7-2925-46cd-b6cf-ad467c784e7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 20,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 116,
                "y": 24
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "9f27a85a-2eef-4127-a00d-07ba439e5600",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 20,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 211,
                "y": 24
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "3698e8c3-9094-438d-8951-e9b09187de70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 20,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 106,
                "y": 24
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "4f17bf2d-155d-4ca0-8289-84fb73a95588",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 84,
                "y": 24
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "e0e2d479-9729-42f9-8bd0-23df9d885013",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 20,
                "offset": -1,
                "shift": 8,
                "w": 9,
                "x": 73,
                "y": 24
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "82497724-3aeb-42cb-90d1-0e15193f4f7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 61,
                "y": 24
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "42ec51fd-fe8a-487b-8f5b-eb4d8952feef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 50,
                "y": 24
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "5dfa6897-d073-4dbb-8b4d-98eea81b8a15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 20,
                "offset": -1,
                "shift": 8,
                "w": 9,
                "x": 39,
                "y": 24
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "fdbd37c1-d747-4fb5-9dd2-04e61a4bec02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 30,
                "y": 24
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "10b05f64-cea5-4289-9af2-4a9c7bb115a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 21,
                "y": 24
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "7b37cb24-64b0-4b47-b5e8-2c5e8725ef70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 20,
                "offset": 4,
                "shift": 10,
                "w": 2,
                "x": 17,
                "y": 24
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "1313c650-0989-4b63-9e07-3acb201bf597",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 8,
                "y": 24
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "8be49ee0-108b-424f-8311-eb00adfed2fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 190,
                "y": 68
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "1740ada0-31b9-4744-8ed4-96ce8b9020b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 200,
                "y": 68
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\n0123456789 .,<>\"'&!?\\nthe quick brown fox jumps over the lazy dog\\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\nDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Roman",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}