{
    "id": "06c7c204-77dd-4c71-a376-aedc562cc3cc",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_baby_blocks",
    "AntiAlias": 0,
    "TTFName": null,
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "ChevyRay - Babyblocks",
    "glyphOperations": 3,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "7a9be67f-bdf2-4465-9bf7-6147c9b40909",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 11,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "dd2bb967-e8e7-4774-bc1d-502eea5698d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 11,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 42,
                "y": 41
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "cdcad31c-23bd-44d1-be32-4b20e3aa2da5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 36,
                "y": 41
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "f3d44c73-452b-4584-bbc0-d197180b981e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 11,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 27,
                "y": 41
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "514dfa33-c992-42c2-a486-49282186c579",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 11,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 18,
                "y": 41
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "e1632df3-0d0b-4f6d-977d-95b4de534fcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 11,
                "y": 41
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "1166c690-3643-4ec2-939e-ae73028bbf55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 11,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 41
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "091e33f2-01b9-4717-a73b-67f798511336",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 11,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 121,
                "y": 28
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "6f06b9ec-61b1-4300-b76d-3d97223d8b94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 11,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 116,
                "y": 28
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "68692842-64f6-4f21-a3d7-49f8548cae9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 11,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 111,
                "y": 28
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "8e632be4-6bd1-4591-a097-13e025194ef2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 45,
                "y": 41
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "270864bf-8447-4033-94a2-c8b338ac1db8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 105,
                "y": 28
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "d50ce409-8fe4-4051-8558-1fed3dd4a6eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 11,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 94,
                "y": 28
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "c041c721-fb11-44c9-9993-787661ef82d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 88,
                "y": 28
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "1622692f-0aec-4883-8ea6-962cd739a235",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 11,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 85,
                "y": 28
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "d5da0c8f-7ac5-4b8c-b1aa-26e90926e9b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 78,
                "y": 28
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "91407d51-cd2d-40c6-9f40-dc9346c6ab76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 72,
                "y": 28
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "6a0bb381-afec-4a86-9fc2-dfff849f2140",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 66,
                "y": 28
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "fbd14f9a-bd82-41b0-847e-0e32e613997d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 60,
                "y": 28
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "90b2e1ad-35ab-4ca0-9c79-6aca0679b316",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 54,
                "y": 28
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "9de91d34-94bf-4c7d-971f-c13e82f717f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 48,
                "y": 28
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "180423ce-f524-431e-a4c5-7628a771cf98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 99,
                "y": 28
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "46d7f890-d5ee-47c7-9999-49e53cd07ff5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 51,
                "y": 41
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "f3277bf2-d70e-4c00-aaf6-717ad5857d02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 57,
                "y": 41
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "5ab2ceaa-7f2d-4fce-b806-235d2219db07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 63,
                "y": 41
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "1813909b-f023-413a-9642-b0978338834e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 65,
                "y": 54
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "3feaf039-5ae7-43fc-8b45-ca7f1c062498",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 11,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 62,
                "y": 54
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "874be461-b3c2-4674-afef-eb5702d4b6dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 11,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 57,
                "y": 54
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "15418690-b969-4cf8-89b2-8f2c0b44ccf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 51,
                "y": 54
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "f803422f-9509-4481-b185-0fe4f9d94f71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 45,
                "y": 54
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "f4e3e60c-9b3f-4e30-8d04-5ee9ff5719af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 39,
                "y": 54
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "70b8e332-43fa-4ec3-bc0f-e61a7f44a999",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 33,
                "y": 54
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "adeb6f80-c2df-4bdf-838f-0582b13a2cbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 26,
                "y": 54
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "84988c6c-4d66-44d1-a7d1-9424df702b9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 20,
                "y": 54
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "0e8dbca4-3130-49b6-be13-44d52a5c59bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 14,
                "y": 54
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "05eda5c9-b112-421d-8b8c-c529dcbf11da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 8,
                "y": 54
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "c3304bab-430c-466a-b436-ec853c012e16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 2,
                "y": 54
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "aba18d78-c69f-4ca6-b735-921036993b03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 117,
                "y": 41
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "36ea8032-e00a-4cd1-9996-61b134d0b71f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 111,
                "y": 41
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "654bb180-ab66-4970-bae3-92d2fcfc812e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 105,
                "y": 41
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "9dc24205-be11-40ca-9863-d5513dd1762d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 99,
                "y": 41
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "5e0b9534-b1b7-4e91-8cf8-19aa9906a253",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 11,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 96,
                "y": 41
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "33a71da3-7017-4f26-b6a0-5588ae1db51d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 90,
                "y": 41
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "53631df8-3b8b-4e65-81a6-d4e3980d30b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 84,
                "y": 41
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "3ce099fe-93cd-4c3b-8467-7b7df46be1bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 78,
                "y": 41
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "31f30011-86b1-4deb-80ec-43b410093efe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 11,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 69,
                "y": 41
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "4a3b2a5f-200d-40fb-8d0e-bf2051072acc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 41,
                "y": 28
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "90971e78-95bc-4911-8882-4639974cc291",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 35,
                "y": 28
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "8758092a-cd5a-4dc7-8144-ea426ead7976",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 29,
                "y": 28
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "21508303-51ce-47fd-a1fa-a6645d74e48f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 13,
                "y": 15
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "0a09a490-3c88-40b2-9977-f98cfa0cf370",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 2,
                "y": 15
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "bc32cc9d-9045-4fc5-9ff9-e0930e55b236",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 118,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "06c9c8af-ad55-423c-83c6-871e42cec4ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "6c45b4f0-87c1-4c1d-9ea2-8d55172b45e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "8d87617d-8190-4312-87a8-7a9e6eb622bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "28f8d5f3-4865-4a3e-9002-128a13acc9fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 11,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "cae0eed7-c13e-443d-8d13-db611a6ab6ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 85,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "bf801ddb-9460-45b2-895c-84b50dd0ab27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "95f5173b-3fc0-4b70-9909-6985d01720bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "9a776521-c811-45c1-b808-176ae62a812f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 11,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 8,
                "y": 15
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "5021d039-adf3-47d6-ba5b-c2dda4ca16f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "4edf3190-77ff-48a6-aad8-a21d51772f26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 11,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "1c9ad6cc-e753-4363-a396-bf6728d9d6d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "8a49045f-e7bc-4f1c-a8ec-919c5e373f4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "83983ba9-15dd-48c6-b4f8-2c72b88627c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 11,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "f5e660c1-4117-40bc-81c7-544a121d8cd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "ebfec856-c175-4044-bc95-a4996de78ced",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "7b817f58-7096-4a9e-8040-129a9fdd634d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "f1e59d76-c9af-437a-90c6-a33d8d77a4df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "767cdfb7-1b8c-442f-aeb3-c06973048b7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "51622521-aef7-4f6e-842b-1995dd4b61e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 11,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "025bcea6-a0e8-488c-bf90-693f5e4229f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 19,
                "y": 15
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "2253fcf4-50b3-4ce4-9aaa-89170f4da679",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 81,
                "y": 15
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "a01d4d39-eafc-4261-923b-151fe65d0574",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 11,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 25,
                "y": 15
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "5f7a30c2-5922-4145-8ce7-bc94ca6f1a9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 11,
                "offset": -1,
                "shift": 2,
                "w": 2,
                "x": 20,
                "y": 28
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "6df28f23-efec-4aef-9c25-3b859242db07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 14,
                "y": 28
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "e5ee0e62-7298-4cfe-8f5a-3f8bf9e76b08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 11,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 11,
                "y": 28
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "18cbdde5-eb2c-4584-8d05-84e815166e09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 11,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 28
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "f21e8869-db38-4c11-9fa4-42b464daa75f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 118,
                "y": 15
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "457cf287-76e1-47e9-8a19-85b737c95218",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 112,
                "y": 15
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "0dae483a-58ec-4a4c-bf1d-0575e3f1d39f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 106,
                "y": 15
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "f6aaab8f-f2c3-4cf8-98f5-91ef1d953d93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 99,
                "y": 15
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "f3abd41b-9fe0-448e-beb6-2861eb21b8fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 93,
                "y": 15
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "70950ebc-5096-4c69-b994-d38064f1a75f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 11,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 24,
                "y": 28
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "6fa2c19d-4fb3-4b1b-b8e7-33eae3457691",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 87,
                "y": 15
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "6699cf10-e69f-44ef-978b-63116938e68f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 75,
                "y": 15
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "8e4ffbd2-2dfa-4252-b4de-5782c5eeaab3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 69,
                "y": 15
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "9983cfe1-f03b-4ecb-bd71-59ed64404101",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 11,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 60,
                "y": 15
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "caf2a31e-7b0f-4797-b0e1-430268dce4a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 54,
                "y": 15
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "283cf298-f70a-49ff-9bea-2d51794baaa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 48,
                "y": 15
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "bdc52f21-1e2d-4e0f-b607-ce5d204d2aef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 11,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 43,
                "y": 15
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "ee06f6f4-412c-45a7-a5e8-d12418a36573",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 37,
                "y": 15
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "c2417fac-d4ae-4fba-b817-623141806d41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 11,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 34,
                "y": 15
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "4eb7815d-2968-4926-bc17-d728293a8b40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 28,
                "y": 15
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "f3b5c903-a0f8-482b-ab94-72b6bf52d63f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 71,
                "y": 54
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "4c05a665-7be0-4919-b704-e6e972783004",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 78,
                "y": 54
            }
        }
    ],
    "hinting": 2,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "fe15ca54-5fbb-4f71-8d1d-53301f085bde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 52
        },
        {
            "id": "d3abaa96-6162-4d1e-8e30-ea4244595eb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 55
        },
        {
            "id": "4c2aad64-0655-49b4-b682-5cef460d5d11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 57
        },
        {
            "id": "d0c68ec9-661d-4f2b-862e-d38931bfd3ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 55
        },
        {
            "id": "14ae852d-6379-4cfd-bec8-81473b19b6b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 84
        },
        {
            "id": "6b108656-fc52-4bfb-8397-1b64a6ce9208",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 55
        },
        {
            "id": "ecea1bbc-1324-4557-8594-1647af815139",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 57
        },
        {
            "id": "c20e8ca7-a596-40f2-81f9-a7c2b07b7847",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "04c6d226-a67e-4fcd-98e6-4b040306f068",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "4e298f10-2c66-452d-bc8d-1e3b9351db5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 118
        },
        {
            "id": "3ddfd0df-09b1-4ee2-88df-396ac6d0d0df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 119
        },
        {
            "id": "3241c3d4-433e-493c-8c08-78a9ef780aa4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "7c145360-b251-4c3c-9613-bdd2a45c0a2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "a62ddb86-331d-4f23-a0ca-73e8decc2462",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 100
        },
        {
            "id": "9e17060f-f90d-42b4-9025-8dc3c9f54555",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "2cef6fde-74c1-45fe-9ab3-8d49e102c5be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 103
        },
        {
            "id": "04c9ff45-6294-443a-9909-53d2f6f8fe10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 109
        },
        {
            "id": "15356a07-fb3f-44a1-9550-66a4d3baa80e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 110
        },
        {
            "id": "bfb44429-4e30-40b6-8283-072da38facd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "d17d33c0-dd45-4c97-af4e-f9ed57732cf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 112
        },
        {
            "id": "a4ad74f5-dab9-4c35-a99f-9e55914c8ae3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 113
        },
        {
            "id": "869f6720-8fae-4e9b-9b59-ef84eb54bc2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "f0028c67-8d08-48fb-8e5d-d79214167604",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "cf645152-1881-4c65-92e9-f239b16df78d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "f5a39833-c971-4378-9f1d-bbce72ddc362",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 118
        },
        {
            "id": "3fdb7a22-695c-4845-b1e0-b910e2472c59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "2f15122a-68eb-4fdb-adf6-4804eceea7b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 120
        },
        {
            "id": "52135ce9-97c7-4125-bc41-9e912ed5f2f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "85ed0563-23a3-44ea-9f54-f42033b36965",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 122
        },
        {
            "id": "ec3e34e2-ea80-4e4b-8c5d-1f5fbf0260ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 84
        },
        {
            "id": "000f8b10-1e7f-499e-9b31-2bea2dada078",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 85
        },
        {
            "id": "c81a5b6b-e465-4042-b95b-a4ce2a8b81a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 86
        },
        {
            "id": "693aa939-37e1-41e4-83c6-4ca160fb25ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 111
        },
        {
            "id": "3f70906b-d874-4e2b-a828-749dae576f31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 118
        },
        {
            "id": "309681a5-e198-4da1-908f-eacdca98c62a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 84
        },
        {
            "id": "f117eaec-5e5c-4759-a3ff-78d0bd0a2d57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 84
        },
        {
            "id": "de232b27-e05c-43b1-9ea3-830cf189e4d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 84
        },
        {
            "id": "805dc963-8e86-49a7-9b4b-c5c20ecd2b0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 84
        },
        {
            "id": "678c2277-ca56-4cb6-99b1-bb8863e5b20e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 84
        },
        {
            "id": "f7995c32-8d1d-41ac-89fb-649c7f5b5daa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 84
        },
        {
            "id": "162d836c-a131-4e27-951c-44dc7d651ae5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 84
        },
        {
            "id": "0a42ad0c-bed9-488e-807f-8e6649ae3549",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 84
        },
        {
            "id": "214ad0a6-4e98-4976-8ad4-3a48ad927bd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 84
        },
        {
            "id": "b2d85e31-5d9d-4296-80f1-de784fc79db1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 84
        },
        {
            "id": "ebd4dbd7-dc22-4ba4-b96c-c28b3ce30030",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 84
        },
        {
            "id": "08d506c4-49e3-438e-9946-40f0b9636fed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 106
        },
        {
            "id": "2ee5f46c-ca1b-477d-b5d3-84733760d991",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 84
        },
        {
            "id": "9fbad1fc-b813-497a-a318-3e7e7c07f3b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 84
        },
        {
            "id": "d47dc55f-094b-4b1b-bf67-f7c09737cdea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 84
        },
        {
            "id": "d93e4545-537d-4ebd-89e9-89e1a8e09975",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 84
        },
        {
            "id": "49b14ea7-f338-4015-9632-008cefa23436",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 84
        },
        {
            "id": "5a2327dd-e553-4825-8995-a464536978be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 84
        },
        {
            "id": "08dc67ca-2463-475e-83bd-8249dabcc5db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 84
        },
        {
            "id": "32d1420b-f7f9-4e96-b5b4-68263adc1c04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 84
        },
        {
            "id": "5fc180ce-3311-4b96-81d4-e41786894aa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 84
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\n0123456789 .,<>\"'&!?\\nthe quick brown fox jumps over the lazy dog\\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\nDefault character: ▯ (9647)",
    "size": 8,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}