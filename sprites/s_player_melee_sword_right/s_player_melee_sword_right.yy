{
    "id": "69e7b11b-d968-4290-a707-2bf12a808666",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_melee_sword_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 6,
    "bbox_right": 54,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5af86f8d-96a8-4c9e-af99-d173f4c6ac44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69e7b11b-d968-4290-a707-2bf12a808666",
            "compositeImage": {
                "id": "5be03f49-8b50-47af-902b-bac3f665f38e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5af86f8d-96a8-4c9e-af99-d173f4c6ac44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f9ecf44-c629-4ac5-8464-ba5f93f76435",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5af86f8d-96a8-4c9e-af99-d173f4c6ac44",
                    "LayerId": "61b15988-743a-4c2c-981e-cb9438c41a62"
                }
            ]
        },
        {
            "id": "7d5ea9fc-f0d2-471b-92e3-5ccca829993d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69e7b11b-d968-4290-a707-2bf12a808666",
            "compositeImage": {
                "id": "40c98128-1535-4717-86ca-757f77e83577",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d5ea9fc-f0d2-471b-92e3-5ccca829993d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ea87fde-3d0a-4444-a0f8-d7c455ddb359",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d5ea9fc-f0d2-471b-92e3-5ccca829993d",
                    "LayerId": "61b15988-743a-4c2c-981e-cb9438c41a62"
                }
            ]
        },
        {
            "id": "84ce5716-7889-4dde-a674-f1a03492beb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69e7b11b-d968-4290-a707-2bf12a808666",
            "compositeImage": {
                "id": "6fcc71d3-ab7a-4f76-bfa0-6b3b11ec14a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84ce5716-7889-4dde-a674-f1a03492beb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4ae65e5-3b98-44e7-87b4-4f2be829f373",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84ce5716-7889-4dde-a674-f1a03492beb4",
                    "LayerId": "61b15988-743a-4c2c-981e-cb9438c41a62"
                }
            ]
        },
        {
            "id": "a455d53d-e2fe-45d8-b854-0123683c8ee8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69e7b11b-d968-4290-a707-2bf12a808666",
            "compositeImage": {
                "id": "9c2a4dc0-68eb-4fcf-9c6f-9f6aa34dd24b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a455d53d-e2fe-45d8-b854-0123683c8ee8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aaf4a99e-22f7-42c5-9dd7-e1d4366360b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a455d53d-e2fe-45d8-b854-0123683c8ee8",
                    "LayerId": "61b15988-743a-4c2c-981e-cb9438c41a62"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "61b15988-743a-4c2c-981e-cb9438c41a62",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "69e7b11b-d968-4290-a707-2bf12a808666",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 19,
    "yorig": 47
}