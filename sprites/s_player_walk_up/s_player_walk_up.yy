{
    "id": "d06d9fb3-8cc2-4b85-ad81-3359e40ddb18",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_walk_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 6,
    "bbox_right": 23,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e7a91cf1-a2e7-4a9f-80a8-15cf6a4bee14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d06d9fb3-8cc2-4b85-ad81-3359e40ddb18",
            "compositeImage": {
                "id": "51bca2ea-a56b-4b18-84b5-5df35475d124",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7a91cf1-a2e7-4a9f-80a8-15cf6a4bee14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "088e2005-999a-46a3-8092-f5152dc4e5f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7a91cf1-a2e7-4a9f-80a8-15cf6a4bee14",
                    "LayerId": "e1ed218b-ce53-4def-ba44-8eac83c9546b"
                }
            ]
        },
        {
            "id": "8dad036d-5771-4d21-bc58-448d7334608b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d06d9fb3-8cc2-4b85-ad81-3359e40ddb18",
            "compositeImage": {
                "id": "51ccbeec-cc40-4319-8eca-d115d39ba38c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8dad036d-5771-4d21-bc58-448d7334608b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e73cd20-1df6-4072-844c-106e81d0a11d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8dad036d-5771-4d21-bc58-448d7334608b",
                    "LayerId": "e1ed218b-ce53-4def-ba44-8eac83c9546b"
                }
            ]
        },
        {
            "id": "85f5efbe-147a-48cc-93ed-610a53039d1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d06d9fb3-8cc2-4b85-ad81-3359e40ddb18",
            "compositeImage": {
                "id": "4cb59b56-0e63-4893-b05d-b42ea2e18a7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85f5efbe-147a-48cc-93ed-610a53039d1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad8e06e0-97d8-4580-91d4-d4278929e98e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85f5efbe-147a-48cc-93ed-610a53039d1a",
                    "LayerId": "e1ed218b-ce53-4def-ba44-8eac83c9546b"
                }
            ]
        },
        {
            "id": "ae3dadd3-cc36-40aa-9c33-0354aec1a832",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d06d9fb3-8cc2-4b85-ad81-3359e40ddb18",
            "compositeImage": {
                "id": "497d5572-e76b-4d31-8978-aa6f2d6f574e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae3dadd3-cc36-40aa-9c33-0354aec1a832",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9101871f-108b-4488-8787-173a3898b244",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae3dadd3-cc36-40aa-9c33-0354aec1a832",
                    "LayerId": "e1ed218b-ce53-4def-ba44-8eac83c9546b"
                }
            ]
        },
        {
            "id": "45d43704-b4dc-4c1f-9f25-463c49bae828",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d06d9fb3-8cc2-4b85-ad81-3359e40ddb18",
            "compositeImage": {
                "id": "fb06dfb3-b024-429c-8842-b928025fd823",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45d43704-b4dc-4c1f-9f25-463c49bae828",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4706261-14f0-4a00-bf56-1bab9efa564a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45d43704-b4dc-4c1f-9f25-463c49bae828",
                    "LayerId": "e1ed218b-ce53-4def-ba44-8eac83c9546b"
                }
            ]
        },
        {
            "id": "7462b368-9702-4543-b7ab-ebfeeae3d0c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d06d9fb3-8cc2-4b85-ad81-3359e40ddb18",
            "compositeImage": {
                "id": "d1503a8e-07f6-4b47-a631-5c9fa02da9e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7462b368-9702-4543-b7ab-ebfeeae3d0c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9014c4e4-6241-4aee-8c62-cc0e93c1c2a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7462b368-9702-4543-b7ab-ebfeeae3d0c5",
                    "LayerId": "e1ed218b-ce53-4def-ba44-8eac83c9546b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e1ed218b-ce53-4def-ba44-8eac83c9546b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d06d9fb3-8cc2-4b85-ad81-3359e40ddb18",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}