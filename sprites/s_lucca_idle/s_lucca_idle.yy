{
    "id": "86eddbf6-fd31-4dbe-8714-40508b76b47a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_lucca_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 8,
    "bbox_right": 23,
    "bbox_top": 23,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9085e2ab-85ac-4fec-aee3-70b76f660add",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86eddbf6-fd31-4dbe-8714-40508b76b47a",
            "compositeImage": {
                "id": "99812a02-4fbb-4c83-95e7-8a0bbba13b70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9085e2ab-85ac-4fec-aee3-70b76f660add",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31ffdd83-ad47-42d8-9821-3c6a90b46e93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9085e2ab-85ac-4fec-aee3-70b76f660add",
                    "LayerId": "d40e6524-e2c1-4a93-b351-7ebb030bd585"
                }
            ]
        },
        {
            "id": "ca71c394-61a7-44e8-b734-12d0fe845ca5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86eddbf6-fd31-4dbe-8714-40508b76b47a",
            "compositeImage": {
                "id": "216c7d7f-519c-4fc7-9e78-94580f9d7d97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca71c394-61a7-44e8-b734-12d0fe845ca5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc13cadd-8acf-4dff-a86f-7558539b258d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca71c394-61a7-44e8-b734-12d0fe845ca5",
                    "LayerId": "d40e6524-e2c1-4a93-b351-7ebb030bd585"
                }
            ]
        },
        {
            "id": "17d12e5e-6290-449a-809d-45ba12ce5777",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86eddbf6-fd31-4dbe-8714-40508b76b47a",
            "compositeImage": {
                "id": "6eaf165a-b270-4f84-87bd-393f9ba8dc31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17d12e5e-6290-449a-809d-45ba12ce5777",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e2b399c-077e-4dc3-829f-fe33bf460c82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17d12e5e-6290-449a-809d-45ba12ce5777",
                    "LayerId": "d40e6524-e2c1-4a93-b351-7ebb030bd585"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d40e6524-e2c1-4a93-b351-7ebb030bd585",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "86eddbf6-fd31-4dbe-8714-40508b76b47a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}