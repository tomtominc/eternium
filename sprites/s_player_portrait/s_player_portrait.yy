{
    "id": "1701e2b9-2767-4e17-b8c8-c1cafb1792ef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_portrait",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0fb7f20c-187e-4381-a5c1-97c7d5165a6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1701e2b9-2767-4e17-b8c8-c1cafb1792ef",
            "compositeImage": {
                "id": "20649211-2b2c-4801-9fdb-a8ae25d729e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fb7f20c-187e-4381-a5c1-97c7d5165a6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fae0818-30d0-4587-ab8d-1ae0b701b7b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fb7f20c-187e-4381-a5c1-97c7d5165a6e",
                    "LayerId": "f1e42761-da97-442c-8fff-d7bd08d26c14"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "f1e42761-da97-442c-8fff-d7bd08d26c14",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1701e2b9-2767-4e17-b8c8-c1cafb1792ef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}