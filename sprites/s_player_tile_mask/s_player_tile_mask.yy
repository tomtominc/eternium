{
    "id": "bab02be0-171e-4671-9b83-3cbcf3588138",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_tile_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 4,
    "bbox_right": 25,
    "bbox_top": 24,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cd3e4a86-9910-49f8-a0d1-7758c575b919",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bab02be0-171e-4671-9b83-3cbcf3588138",
            "compositeImage": {
                "id": "9e5db1bc-e4e2-44f8-8483-62e21e647a2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd3e4a86-9910-49f8-a0d1-7758c575b919",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19ce63ef-19df-46bb-ae25-b27e85a0dfdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd3e4a86-9910-49f8-a0d1-7758c575b919",
                    "LayerId": "8af38fdd-d342-4a6f-80ac-0d7c50b74bdd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8af38fdd-d342-4a6f-80ac-0d7c50b74bdd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bab02be0-171e-4671-9b83-3cbcf3588138",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}