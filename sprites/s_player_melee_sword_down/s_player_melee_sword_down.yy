{
    "id": "bc81125e-d05b-4050-98b7-181aa05b172d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_melee_sword_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 0,
    "bbox_right": 60,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "adffd3c4-0d1a-416d-ab82-70b175d77148",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc81125e-d05b-4050-98b7-181aa05b172d",
            "compositeImage": {
                "id": "2028da7b-cb08-449b-a505-784d473a4c69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adffd3c4-0d1a-416d-ab82-70b175d77148",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09714838-c04a-4814-b235-686043a36278",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adffd3c4-0d1a-416d-ab82-70b175d77148",
                    "LayerId": "9f155181-bb50-4ce6-8806-9b75d073c81f"
                }
            ]
        },
        {
            "id": "e98c6ab4-73ac-4ca8-b158-33fd74de3ade",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc81125e-d05b-4050-98b7-181aa05b172d",
            "compositeImage": {
                "id": "94bd26c7-699b-4c10-9418-0ce16996e51a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e98c6ab4-73ac-4ca8-b158-33fd74de3ade",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7a22607-23c4-48ca-9aba-d814db8fa3ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e98c6ab4-73ac-4ca8-b158-33fd74de3ade",
                    "LayerId": "9f155181-bb50-4ce6-8806-9b75d073c81f"
                }
            ]
        },
        {
            "id": "0389b4da-544e-4124-96c3-3f37d90974c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc81125e-d05b-4050-98b7-181aa05b172d",
            "compositeImage": {
                "id": "5ddc2762-19a9-442c-a07b-ead0cc222f05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0389b4da-544e-4124-96c3-3f37d90974c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "665c2621-b61b-4943-9163-1ba585237f94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0389b4da-544e-4124-96c3-3f37d90974c8",
                    "LayerId": "9f155181-bb50-4ce6-8806-9b75d073c81f"
                }
            ]
        },
        {
            "id": "a8f1421a-7942-45c2-9112-09267feed74e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc81125e-d05b-4050-98b7-181aa05b172d",
            "compositeImage": {
                "id": "eea72b11-0e58-4ad9-9c16-d78a1f1a402d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8f1421a-7942-45c2-9112-09267feed74e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "202bfc0c-e2e9-40f9-b8cd-94bd61db0c04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8f1421a-7942-45c2-9112-09267feed74e",
                    "LayerId": "9f155181-bb50-4ce6-8806-9b75d073c81f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9f155181-bb50-4ce6-8806-9b75d073c81f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc81125e-d05b-4050-98b7-181aa05b172d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 47
}