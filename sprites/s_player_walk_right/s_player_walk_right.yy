{
    "id": "af3a7e6a-9ef4-4d5c-86a6-336d81913db4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_walk_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 4,
    "bbox_right": 24,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a9093e53-341c-440e-b255-e1a325d0031a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af3a7e6a-9ef4-4d5c-86a6-336d81913db4",
            "compositeImage": {
                "id": "d35332f6-4fd7-4de8-bdf5-aed03acca9f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9093e53-341c-440e-b255-e1a325d0031a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e5b87b6-917c-4f3e-bfb8-3b84598ed513",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9093e53-341c-440e-b255-e1a325d0031a",
                    "LayerId": "e55d87e5-edc7-4bbb-adfc-bcefad245ae9"
                }
            ]
        },
        {
            "id": "ed22f1e3-166f-4b75-b66b-86ccf70079b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af3a7e6a-9ef4-4d5c-86a6-336d81913db4",
            "compositeImage": {
                "id": "8cf08953-552e-46e8-98b9-17778e26c48b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed22f1e3-166f-4b75-b66b-86ccf70079b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7db00f57-32b5-43ce-8f56-d8cb1912bd3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed22f1e3-166f-4b75-b66b-86ccf70079b3",
                    "LayerId": "e55d87e5-edc7-4bbb-adfc-bcefad245ae9"
                }
            ]
        },
        {
            "id": "8b1105af-f756-42eb-a30b-d62f54785c1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af3a7e6a-9ef4-4d5c-86a6-336d81913db4",
            "compositeImage": {
                "id": "0ddbd6c5-ee09-4b77-a8c3-800064dbf79a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b1105af-f756-42eb-a30b-d62f54785c1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2520626f-4664-47f6-86f3-9185f3e30dcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b1105af-f756-42eb-a30b-d62f54785c1a",
                    "LayerId": "e55d87e5-edc7-4bbb-adfc-bcefad245ae9"
                }
            ]
        },
        {
            "id": "7c42d111-e625-4673-b68e-aa5d3d7ef3a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af3a7e6a-9ef4-4d5c-86a6-336d81913db4",
            "compositeImage": {
                "id": "6f696473-50b2-48e5-a495-af72f7bb0375",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c42d111-e625-4673-b68e-aa5d3d7ef3a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0361025-54aa-4faa-8a4c-81d8aea0eeed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c42d111-e625-4673-b68e-aa5d3d7ef3a5",
                    "LayerId": "e55d87e5-edc7-4bbb-adfc-bcefad245ae9"
                }
            ]
        },
        {
            "id": "7d0af885-aaba-46d5-bb48-4567a748382f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af3a7e6a-9ef4-4d5c-86a6-336d81913db4",
            "compositeImage": {
                "id": "e69c3c1e-e201-43dc-bbf3-f7730fddc656",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d0af885-aaba-46d5-bb48-4567a748382f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af542a7c-c4ff-4a3f-a6e3-27215b300ce3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d0af885-aaba-46d5-bb48-4567a748382f",
                    "LayerId": "e55d87e5-edc7-4bbb-adfc-bcefad245ae9"
                }
            ]
        },
        {
            "id": "2b7b4d6b-d2db-4ebf-b9eb-866d4bcf7968",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af3a7e6a-9ef4-4d5c-86a6-336d81913db4",
            "compositeImage": {
                "id": "09e53143-e10d-4781-8624-0db677791b60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b7b4d6b-d2db-4ebf-b9eb-866d4bcf7968",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ae4a90e-afd2-4880-b083-6dab94457f20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b7b4d6b-d2db-4ebf-b9eb-866d4bcf7968",
                    "LayerId": "e55d87e5-edc7-4bbb-adfc-bcefad245ae9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e55d87e5-edc7-4bbb-adfc-bcefad245ae9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af3a7e6a-9ef4-4d5c-86a6-336d81913db4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}