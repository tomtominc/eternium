{
    "id": "a6246691-3b2b-4475-aff2-0e22dff29e1f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_jump_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "85746cc3-5a1c-4f4b-9ea9-dfc9a394f624",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6246691-3b2b-4475-aff2-0e22dff29e1f",
            "compositeImage": {
                "id": "3b2b2227-8220-425c-8e10-6310792e0170",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85746cc3-5a1c-4f4b-9ea9-dfc9a394f624",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4bd0646-4b98-412c-89e3-d3aea9f527e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85746cc3-5a1c-4f4b-9ea9-dfc9a394f624",
                    "LayerId": "c5087381-8887-44e8-a061-9ec6b5daa47d"
                }
            ]
        },
        {
            "id": "53babf61-85ac-400e-becb-ed1acacbaacc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6246691-3b2b-4475-aff2-0e22dff29e1f",
            "compositeImage": {
                "id": "32a0a481-d6d7-44b7-b63e-f89e9d4a8e82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53babf61-85ac-400e-becb-ed1acacbaacc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "830c778c-d51b-42db-9d46-5cfaabb3ba26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53babf61-85ac-400e-becb-ed1acacbaacc",
                    "LayerId": "c5087381-8887-44e8-a061-9ec6b5daa47d"
                }
            ]
        },
        {
            "id": "71dff2c2-29fe-4876-805b-de66fc237269",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6246691-3b2b-4475-aff2-0e22dff29e1f",
            "compositeImage": {
                "id": "79b2dd26-d4d0-4f34-82ba-69fb5237d130",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71dff2c2-29fe-4876-805b-de66fc237269",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1940419a-604b-4bc7-9c77-781949dd835b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71dff2c2-29fe-4876-805b-de66fc237269",
                    "LayerId": "c5087381-8887-44e8-a061-9ec6b5daa47d"
                }
            ]
        },
        {
            "id": "be7a8b5e-521c-4583-910d-9c6042052383",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6246691-3b2b-4475-aff2-0e22dff29e1f",
            "compositeImage": {
                "id": "573847fe-3f95-4431-b128-f4c1d6844ecb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be7a8b5e-521c-4583-910d-9c6042052383",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51d871cc-5b49-4ba8-8480-f4b8a8e77441",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be7a8b5e-521c-4583-910d-9c6042052383",
                    "LayerId": "c5087381-8887-44e8-a061-9ec6b5daa47d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c5087381-8887-44e8-a061-9ec6b5daa47d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a6246691-3b2b-4475-aff2-0e22dff29e1f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}