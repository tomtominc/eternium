{
    "id": "83e2fbd4-42c9-42ba-9c31-6e4518f1e65f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_shadow_16x16",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b649345-9e27-44a1-b86a-6fe15dfdbbd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83e2fbd4-42c9-42ba-9c31-6e4518f1e65f",
            "compositeImage": {
                "id": "c5c1c0fd-efb6-472f-bf53-da4e7279db1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b649345-9e27-44a1-b86a-6fe15dfdbbd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36935552-4364-457a-a18b-45a066f45de9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b649345-9e27-44a1-b86a-6fe15dfdbbd6",
                    "LayerId": "289a227f-8029-4ef0-a936-18e1ef3b9578"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "289a227f-8029-4ef0-a936-18e1ef3b9578",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83e2fbd4-42c9-42ba-9c31-6e4518f1e65f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}