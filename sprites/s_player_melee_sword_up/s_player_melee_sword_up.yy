{
    "id": "eef6beb4-0a64-491c-957f-1d876aa44edc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_melee_sword_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 51,
    "bbox_left": 0,
    "bbox_right": 62,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "500de026-c250-41e2-a94e-50e46cb1e138",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eef6beb4-0a64-491c-957f-1d876aa44edc",
            "compositeImage": {
                "id": "12f3bce5-f0c2-4cad-9e23-d138f95d7ecb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "500de026-c250-41e2-a94e-50e46cb1e138",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "999c8546-08d9-4ef2-b739-44bb8b98fb62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "500de026-c250-41e2-a94e-50e46cb1e138",
                    "LayerId": "81914cf8-8fa5-42f5-9a7a-becea21dd02d"
                }
            ]
        },
        {
            "id": "52f95351-5b39-43ab-bc87-77b19f5f1afe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eef6beb4-0a64-491c-957f-1d876aa44edc",
            "compositeImage": {
                "id": "468a04a5-519a-4807-811a-adbbcec0e5d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52f95351-5b39-43ab-bc87-77b19f5f1afe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc6c8e9f-4b4c-4fb4-a393-8b42c03ed9a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52f95351-5b39-43ab-bc87-77b19f5f1afe",
                    "LayerId": "81914cf8-8fa5-42f5-9a7a-becea21dd02d"
                }
            ]
        },
        {
            "id": "96f3dcb0-5780-4330-9933-60669e0d7cfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eef6beb4-0a64-491c-957f-1d876aa44edc",
            "compositeImage": {
                "id": "18c61ccd-9c3a-44d9-b201-93ddb72042c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96f3dcb0-5780-4330-9933-60669e0d7cfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9679fc93-f844-418d-80e5-77dfac3c41c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96f3dcb0-5780-4330-9933-60669e0d7cfa",
                    "LayerId": "81914cf8-8fa5-42f5-9a7a-becea21dd02d"
                }
            ]
        },
        {
            "id": "8ce7ff32-e205-4b95-b4f0-ff814a057d8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eef6beb4-0a64-491c-957f-1d876aa44edc",
            "compositeImage": {
                "id": "58e315a0-9023-48df-930e-88c87c10e689",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ce7ff32-e205-4b95-b4f0-ff814a057d8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50fc4fa5-c71c-4647-9583-1613b1523672",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ce7ff32-e205-4b95-b4f0-ff814a057d8b",
                    "LayerId": "81914cf8-8fa5-42f5-9a7a-becea21dd02d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "81914cf8-8fa5-42f5-9a7a-becea21dd02d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eef6beb4-0a64-491c-957f-1d876aa44edc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 37,
    "yorig": 47
}