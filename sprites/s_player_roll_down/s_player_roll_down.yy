{
    "id": "8ccee6bc-6e17-4034-a4b0-2e5c47b8ac15",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_roll_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 24,
    "bbox_right": 39,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "032690a2-d23b-49b5-bdbd-78d8bddb38f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ccee6bc-6e17-4034-a4b0-2e5c47b8ac15",
            "compositeImage": {
                "id": "6c670371-5a9c-43bf-84b5-1bf059b42301",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "032690a2-d23b-49b5-bdbd-78d8bddb38f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ee57372-9d77-4803-a4f5-0a5fc3f2b8b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "032690a2-d23b-49b5-bdbd-78d8bddb38f6",
                    "LayerId": "5578ea00-c69d-4f4b-b02a-84ffd19f1d82"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5578ea00-c69d-4f4b-b02a-84ffd19f1d82",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ccee6bc-6e17-4034-a4b0-2e5c47b8ac15",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 34
}