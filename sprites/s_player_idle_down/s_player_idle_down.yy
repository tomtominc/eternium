{
    "id": "323f9448-f28d-43c1-a6ac-20a0f54d160a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_idle_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 11,
    "bbox_right": 18,
    "bbox_top": 24,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9db8780c-bead-4e37-99d7-586be03aa4d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "323f9448-f28d-43c1-a6ac-20a0f54d160a",
            "compositeImage": {
                "id": "fd373c97-26ff-4efb-af4e-09a81310f397",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9db8780c-bead-4e37-99d7-586be03aa4d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13a6bc11-ae5f-43f0-9bfe-6e09b42f8cb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9db8780c-bead-4e37-99d7-586be03aa4d7",
                    "LayerId": "8ea69160-4a6f-4933-8107-a51f645a3601"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8ea69160-4a6f-4933-8107-a51f645a3601",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "323f9448-f28d-43c1-a6ac-20a0f54d160a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}