{
    "id": "efd3b174-43e0-4b20-a4ae-5109f297337c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_jump_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 28,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "55baab36-8046-4640-8182-e37e7951f85b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efd3b174-43e0-4b20-a4ae-5109f297337c",
            "compositeImage": {
                "id": "e90a4397-dd7c-4ce1-bfc2-1acdab227c5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55baab36-8046-4640-8182-e37e7951f85b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed3de6de-ba72-4e61-88ba-3600b99fedfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55baab36-8046-4640-8182-e37e7951f85b",
                    "LayerId": "7fc918ea-e92a-46b7-a90e-7e2003b0c041"
                }
            ]
        },
        {
            "id": "4d9aca3d-50d4-43ec-a69a-931db00edd7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efd3b174-43e0-4b20-a4ae-5109f297337c",
            "compositeImage": {
                "id": "0e132d8f-7edd-407a-931f-49223a96b136",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d9aca3d-50d4-43ec-a69a-931db00edd7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1cdb18c-95da-4ebf-9b65-363eb1a4e04f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d9aca3d-50d4-43ec-a69a-931db00edd7d",
                    "LayerId": "7fc918ea-e92a-46b7-a90e-7e2003b0c041"
                }
            ]
        },
        {
            "id": "a43ddae4-fccc-4d01-9ec7-48570f9fe563",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efd3b174-43e0-4b20-a4ae-5109f297337c",
            "compositeImage": {
                "id": "7050ec3f-ca4d-4028-b137-89a441aa1ed1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a43ddae4-fccc-4d01-9ec7-48570f9fe563",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08c9d7fe-8b55-400b-8e16-8714d282eaab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a43ddae4-fccc-4d01-9ec7-48570f9fe563",
                    "LayerId": "7fc918ea-e92a-46b7-a90e-7e2003b0c041"
                }
            ]
        },
        {
            "id": "07bc8dad-c4ad-46cf-9d52-fe92b5bb3fb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efd3b174-43e0-4b20-a4ae-5109f297337c",
            "compositeImage": {
                "id": "052f9526-9b11-4b37-aec5-a4ca05804bfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07bc8dad-c4ad-46cf-9d52-fe92b5bb3fb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "594c50d9-f21c-4c29-9e2e-85914795897c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07bc8dad-c4ad-46cf-9d52-fe92b5bb3fb5",
                    "LayerId": "7fc918ea-e92a-46b7-a90e-7e2003b0c041"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7fc918ea-e92a-46b7-a90e-7e2003b0c041",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "efd3b174-43e0-4b20-a4ae-5109f297337c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}