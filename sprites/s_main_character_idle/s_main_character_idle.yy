{
    "id": "307bfe3f-ecb2-4662-8f66-fed3ff8058a2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_main_character_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 24,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c9749f5-8884-4eb8-b458-be4703ba3fbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "307bfe3f-ecb2-4662-8f66-fed3ff8058a2",
            "compositeImage": {
                "id": "db23dc6b-4f38-4cf6-91a9-a2c46c8bd119",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c9749f5-8884-4eb8-b458-be4703ba3fbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83c3c076-c98e-45e3-8e02-44b688f24e8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c9749f5-8884-4eb8-b458-be4703ba3fbd",
                    "LayerId": "a59f0880-2c59-4075-baaf-9fb2f8e498c6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a59f0880-2c59-4075-baaf-9fb2f8e498c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "307bfe3f-ecb2-4662-8f66-fed3ff8058a2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 31
}