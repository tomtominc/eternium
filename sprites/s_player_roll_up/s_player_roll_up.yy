{
    "id": "43ef7429-cf5b-415b-a8e1-233a036fe1f6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_roll_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 24,
    "bbox_right": 38,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bcd62534-6956-47cf-bab8-ee4044644b67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43ef7429-cf5b-415b-a8e1-233a036fe1f6",
            "compositeImage": {
                "id": "48a3a473-6905-4a66-95da-10676b31abf0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcd62534-6956-47cf-bab8-ee4044644b67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59eba4c4-9248-46f5-8f1b-592a77854196",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcd62534-6956-47cf-bab8-ee4044644b67",
                    "LayerId": "4d4461ca-b2a0-42de-bee3-8f29e9e0187d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4d4461ca-b2a0-42de-bee3-8f29e9e0187d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "43ef7429-cf5b-415b-a8e1-233a036fe1f6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 15
}