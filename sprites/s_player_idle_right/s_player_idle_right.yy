{
    "id": "a9da3fc7-fd04-44e3-b7d7-9ded1facad3b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_idle_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 8,
    "bbox_right": 20,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f96feef4-647d-457f-ba14-704be8a575ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9da3fc7-fd04-44e3-b7d7-9ded1facad3b",
            "compositeImage": {
                "id": "a6fbb5dd-3605-4bce-8fdf-9d3862c74e40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f96feef4-647d-457f-ba14-704be8a575ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6d32142-d5c6-4866-95b0-03f6d2882198",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f96feef4-647d-457f-ba14-704be8a575ce",
                    "LayerId": "5090ab67-660c-4f16-adfd-bf5d767e5610"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5090ab67-660c-4f16-adfd-bf5d767e5610",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a9da3fc7-fd04-44e3-b7d7-9ded1facad3b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}