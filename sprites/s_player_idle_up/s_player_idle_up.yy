{
    "id": "0367ba72-9860-4c4e-a8e3-803248f29d6d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_idle_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 8,
    "bbox_right": 22,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b5c25f4-a73c-4a96-9161-ee8ded43ec4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0367ba72-9860-4c4e-a8e3-803248f29d6d",
            "compositeImage": {
                "id": "eda9cab7-1044-4b1b-bb53-9ca803fca88c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b5c25f4-a73c-4a96-9161-ee8ded43ec4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e20aa30e-728d-4c17-896c-755475685d95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b5c25f4-a73c-4a96-9161-ee8ded43ec4e",
                    "LayerId": "1bb1d4e2-f972-4da1-b17b-f5041799850c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1bb1d4e2-f972-4da1-b17b-f5041799850c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0367ba72-9860-4c4e-a8e3-803248f29d6d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}