{
    "id": "ca3d57a0-7011-48e2-ae46-ac145093accf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_roll_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 11,
    "bbox_right": 52,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "807336b7-7424-43c6-acb7-be32aa61cbd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca3d57a0-7011-48e2-ae46-ac145093accf",
            "compositeImage": {
                "id": "51c53455-9d91-4add-a149-70b27727cabb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "807336b7-7424-43c6-acb7-be32aa61cbd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "289ca68e-0658-4b38-9924-62de39b45913",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "807336b7-7424-43c6-acb7-be32aa61cbd5",
                    "LayerId": "5403468e-49a2-4e15-a7da-64b91ee4a7a9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5403468e-49a2-4e15-a7da-64b91ee4a7a9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca3d57a0-7011-48e2-ae46-ac145093accf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 18
}