{
    "id": "b7a2ee3f-4b84-4629-90b6-bddeed380a0d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_walk_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 7,
    "bbox_right": 22,
    "bbox_top": 24,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4b32307d-61f3-4b0d-b977-17cfcd333781",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7a2ee3f-4b84-4629-90b6-bddeed380a0d",
            "compositeImage": {
                "id": "11443e08-812a-4568-b5eb-a1b21151041f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b32307d-61f3-4b0d-b977-17cfcd333781",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "649d18e9-7d35-4301-b76d-ac4f655384b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b32307d-61f3-4b0d-b977-17cfcd333781",
                    "LayerId": "fa156a85-c458-44e5-a8d2-a47d9b9d85cc"
                }
            ]
        },
        {
            "id": "e93cbe14-ee9a-4033-b05e-8c47507e9e95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7a2ee3f-4b84-4629-90b6-bddeed380a0d",
            "compositeImage": {
                "id": "876a7439-de1f-43cc-a04f-27b6de88a465",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e93cbe14-ee9a-4033-b05e-8c47507e9e95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f58e4f2-6248-44ac-8974-46da0a29fbc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e93cbe14-ee9a-4033-b05e-8c47507e9e95",
                    "LayerId": "fa156a85-c458-44e5-a8d2-a47d9b9d85cc"
                }
            ]
        },
        {
            "id": "86f602e3-047f-4b7d-8955-45aab1e431d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7a2ee3f-4b84-4629-90b6-bddeed380a0d",
            "compositeImage": {
                "id": "60f007c3-e4ce-40d1-bd12-71b73da62e7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86f602e3-047f-4b7d-8955-45aab1e431d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2395f96f-1b18-4cad-9c5f-7f24b60cb46a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86f602e3-047f-4b7d-8955-45aab1e431d7",
                    "LayerId": "fa156a85-c458-44e5-a8d2-a47d9b9d85cc"
                }
            ]
        },
        {
            "id": "f2173a82-f990-4dc7-bf19-af9800e6fb94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7a2ee3f-4b84-4629-90b6-bddeed380a0d",
            "compositeImage": {
                "id": "501ec019-b7fa-4bb1-af2d-9e39db19a901",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2173a82-f990-4dc7-bf19-af9800e6fb94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7dffe205-7794-4e5e-b1ed-8b973b6322c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2173a82-f990-4dc7-bf19-af9800e6fb94",
                    "LayerId": "fa156a85-c458-44e5-a8d2-a47d9b9d85cc"
                }
            ]
        },
        {
            "id": "0116c032-8599-4f92-9c1e-8f629d1d62b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7a2ee3f-4b84-4629-90b6-bddeed380a0d",
            "compositeImage": {
                "id": "f8a1a02a-68c9-4775-8c03-44f8bccade8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0116c032-8599-4f92-9c1e-8f629d1d62b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ad9cb7e-1a7b-47ac-a24d-1dedb3a7e5b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0116c032-8599-4f92-9c1e-8f629d1d62b8",
                    "LayerId": "fa156a85-c458-44e5-a8d2-a47d9b9d85cc"
                }
            ]
        },
        {
            "id": "6c494a8c-9029-4d8d-a89a-f85c7fc0f34a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7a2ee3f-4b84-4629-90b6-bddeed380a0d",
            "compositeImage": {
                "id": "0744cfb5-e052-42cc-b975-9f136ce5164a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c494a8c-9029-4d8d-a89a-f85c7fc0f34a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3f601db-8c9f-48fa-ae40-e62d3e61553d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c494a8c-9029-4d8d-a89a-f85c7fc0f34a",
                    "LayerId": "fa156a85-c458-44e5-a8d2-a47d9b9d85cc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fa156a85-c458-44e5-a8d2-a47d9b9d85cc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b7a2ee3f-4b84-4629-90b6-bddeed380a0d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}