{
    "id": "295681ec-f6f8-47c4-bf39-060f931ea998",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_tileset_prototype",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "62a6d529-b9e8-4607-8a50-33d0d03a5c80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "295681ec-f6f8-47c4-bf39-060f931ea998",
            "compositeImage": {
                "id": "3024eb99-6361-43a8-8315-1bd8b508023d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62a6d529-b9e8-4607-8a50-33d0d03a5c80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a25d3bf4-7ece-4745-b3d4-943b305a80b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62a6d529-b9e8-4607-8a50-33d0d03a5c80",
                    "LayerId": "fd0e2242-658b-4fe2-ae7e-0ba6fe0c7fef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fd0e2242-658b-4fe2-ae7e-0ba6fe0c7fef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "295681ec-f6f8-47c4-bf39-060f931ea998",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}