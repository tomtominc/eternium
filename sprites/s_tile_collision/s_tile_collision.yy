{
    "id": "6cd02cf1-9aff-45ec-9a2f-59b3f45acec6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_tile_collision",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ac5bd627-720a-4af0-9e5e-50d49b62c857",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cd02cf1-9aff-45ec-9a2f-59b3f45acec6",
            "compositeImage": {
                "id": "2fa02d68-02d8-4d3a-9767-e1ca05ce57c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac5bd627-720a-4af0-9e5e-50d49b62c857",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51232b99-41d6-4dbf-91da-082e75df8a22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac5bd627-720a-4af0-9e5e-50d49b62c857",
                    "LayerId": "e4436071-0c4d-4618-abd0-1ff9ce84734c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "e4436071-0c4d-4618-abd0-1ff9ce84734c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6cd02cf1-9aff-45ec-9a2f-59b3f45acec6",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}