{
    "id": "85c24e27-f3fd-42c0-b7f2-b7de3db6da9d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "dust_16x16",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "743c3bc4-b296-48b2-9517-e114acf333de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85c24e27-f3fd-42c0-b7f2-b7de3db6da9d",
            "compositeImage": {
                "id": "e071b8fe-3826-4667-aade-3085190924fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "743c3bc4-b296-48b2-9517-e114acf333de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "609819df-37f4-4072-a5c5-01747320ef25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "743c3bc4-b296-48b2-9517-e114acf333de",
                    "LayerId": "a619fb0a-3f00-43bc-be8d-2ec0e85fce45"
                }
            ]
        },
        {
            "id": "89f60a03-f360-4503-b341-cd5482746f39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85c24e27-f3fd-42c0-b7f2-b7de3db6da9d",
            "compositeImage": {
                "id": "69eea7dd-e636-4ec4-9e79-1d25b7fb4d23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89f60a03-f360-4503-b341-cd5482746f39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6efc9cef-75f9-4a7c-8b0a-547121755c79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89f60a03-f360-4503-b341-cd5482746f39",
                    "LayerId": "a619fb0a-3f00-43bc-be8d-2ec0e85fce45"
                }
            ]
        },
        {
            "id": "d9440dd7-c2ef-4db6-8391-e9b26681ccce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85c24e27-f3fd-42c0-b7f2-b7de3db6da9d",
            "compositeImage": {
                "id": "48033c4b-729f-4a6f-b338-56be9bf19f43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9440dd7-c2ef-4db6-8391-e9b26681ccce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01e1f557-ac98-4aa2-a6ab-31c488ad8579",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9440dd7-c2ef-4db6-8391-e9b26681ccce",
                    "LayerId": "a619fb0a-3f00-43bc-be8d-2ec0e85fce45"
                }
            ]
        },
        {
            "id": "e1d92735-9849-493d-99b3-d9ce824060c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85c24e27-f3fd-42c0-b7f2-b7de3db6da9d",
            "compositeImage": {
                "id": "fce42614-bf15-4093-8e04-03f05d07cdf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1d92735-9849-493d-99b3-d9ce824060c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e0d0c05-a413-4870-8f28-90bb029e353d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1d92735-9849-493d-99b3-d9ce824060c8",
                    "LayerId": "a619fb0a-3f00-43bc-be8d-2ec0e85fce45"
                }
            ]
        },
        {
            "id": "3713bb91-e646-4af3-9d28-912b9714badc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85c24e27-f3fd-42c0-b7f2-b7de3db6da9d",
            "compositeImage": {
                "id": "ab26b2eb-768c-4015-91fc-611f969a2b15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3713bb91-e646-4af3-9d28-912b9714badc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "013aeddb-88b5-41ee-9d38-df7179747ba5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3713bb91-e646-4af3-9d28-912b9714badc",
                    "LayerId": "a619fb0a-3f00-43bc-be8d-2ec0e85fce45"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a619fb0a-3f00-43bc-be8d-2ec0e85fce45",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "85c24e27-f3fd-42c0-b7f2-b7de3db6da9d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 7
}