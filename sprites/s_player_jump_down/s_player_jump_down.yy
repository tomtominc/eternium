{
    "id": "946da03a-e1d7-4779-b6f4-e9294c545a53",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_jump_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f3d1ad76-003a-4d84-9ffd-24280b74d35f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "946da03a-e1d7-4779-b6f4-e9294c545a53",
            "compositeImage": {
                "id": "fb008680-b894-4cde-991e-28222d0f8efb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3d1ad76-003a-4d84-9ffd-24280b74d35f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e918300-ad7d-429c-b357-d9294ac9afe9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3d1ad76-003a-4d84-9ffd-24280b74d35f",
                    "LayerId": "4cca7d25-bb02-4b09-a898-b8f3f567b5f1"
                }
            ]
        },
        {
            "id": "26f51807-26dc-4746-93f6-3caa6a92a4ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "946da03a-e1d7-4779-b6f4-e9294c545a53",
            "compositeImage": {
                "id": "16d9649a-17a1-494a-8387-c887e34cafac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26f51807-26dc-4746-93f6-3caa6a92a4ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed31b62e-3fb7-4a72-9306-23488f255a5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26f51807-26dc-4746-93f6-3caa6a92a4ef",
                    "LayerId": "4cca7d25-bb02-4b09-a898-b8f3f567b5f1"
                }
            ]
        },
        {
            "id": "07ce26fe-ce1d-4fa1-a1ad-2e31885536d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "946da03a-e1d7-4779-b6f4-e9294c545a53",
            "compositeImage": {
                "id": "09fc8a84-526a-4948-a6ec-f58d446afaf8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07ce26fe-ce1d-4fa1-a1ad-2e31885536d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08d46b62-9ec8-4035-996c-2b347d135bfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07ce26fe-ce1d-4fa1-a1ad-2e31885536d3",
                    "LayerId": "4cca7d25-bb02-4b09-a898-b8f3f567b5f1"
                }
            ]
        },
        {
            "id": "a1cf8393-ceba-497c-a239-ac9a910745f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "946da03a-e1d7-4779-b6f4-e9294c545a53",
            "compositeImage": {
                "id": "63494251-2d3e-48d6-8c80-06d6648ca45a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1cf8393-ceba-497c-a239-ac9a910745f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8875f27f-4f40-4669-bc84-d0422a242ae0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1cf8393-ceba-497c-a239-ac9a910745f6",
                    "LayerId": "4cca7d25-bb02-4b09-a898-b8f3f567b5f1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4cca7d25-bb02-4b09-a898-b8f3f567b5f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "946da03a-e1d7-4779-b6f4-e9294c545a53",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}